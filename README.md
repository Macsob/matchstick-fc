# Matchstick FC Project Abstract

Fixed-wing UAVs have superior endurance and horizontal speed over quadrotor designs, but they are unable of operating, taking-off or landing in constricted areas. The tilt rotor bi-copter system studied in this report can take off vertically, hover and transition into horizontal flight. The proposed MAV has the potential to achieve longer flight time than quadcopters due to the fact it generates lift using a fixed wing rather than its rotors in horizontal flight mode. The simplicity of the design is coupled with some control difficulties discussed in the project report.

The main project objective is to design the flight control hardware and firmware for the proposed MAV prototype and demonstrate that the vehicle is operable. The project began with the design and fabrication of the aircraft prototype as well as flight controller hardware, followed by the implementation of sensor fusion and control algorithms in C. Flight tests demonstrate that the MAV can perform vertical take-off and landing manoeuvres and is stable in hover. The vehicle can be safely controlled by the operator with the assistance of the autopilot.

## Flight test videos

Failsafe: https://drive.google.com/file/d/1Mp5rZwJR7Wv8ThY0QvDb8ruRi7mRCO-U/view?usp=sharing

Hand launch: https://drive.google.com/file/d/1Rxpy0g-UwTOsMtYD6w80St-gYb4Fj6g6/view?usp=sharing

Horizontal flight: https://drive.google.com/file/d/1phLdwAVmtqupqIwbEQchr57mfwkWqWI1/view?usp=sharing

Hover and landing: https://drive.google.com/file/d/1efL-CmwIu3sDwDVAXxk1Q0JTDOygZQxZ/view?usp=sharing

Short takeoff: https://drive.google.com/file/d/1seXoBcAygcNN-fPNPAmxdWp57CbBonLq/view?usp=sharing

Vertical takeoff: https://drive.google.com/file/d/1mCMTzo7cNs-8hb4SsQD5Ldkj1uyTfOfq/view?usp=sharing