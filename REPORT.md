## Setup

For bluetooth connection first pair the device, then:

```
$ sudo rfcomm bind 0 98:D3:31:70:80:94
```

Save serial port data to file (0 buffer)

```
$ stdbuf -o0 cat /dev/rfcomm0 > rfcomm0.log
```

Alternative using screen
```
$ screen -L /dev/rfcomm0
```

Install specific compiler version
```
sudo pacman -U https://archive.archlinux.org/packages/a/arm-none-eabi-gcc/arm-none-eabi-gcc-9.3.0-1-x86_64.pkg.tar.zst
```

## Servo signal

Configure time base unit:

    1/72MHz * 72(prescalar) = 1/1MHz;

    for 50Hz: (1 / 1MHz) / (1 / 50Hz) = 20000

    for 100Hz: (1 / 1MHz) / (1 / 100Hz) = 10000

    for 120Hz: (1 / 1MHz) / (1 / 120Hz) = 8330 

    for 200Hz: (1/1Mhz) / ( 1/ 200) = 5000

    for 330Hz: (1/1Mhz) / ( 1/ 330) = 3030

## Progress log

### Preparatory work

19.04.20 20:00 - At high enough RPM there there is something wrong with yaw
estimation (strange offset). Try to eliminate slack on servos maybe. Check if
the same happens in vertical position. Try controlling yaw rates not angles.

20.04.20 16:27 - Didn't notice yesterdays heading problem. Made progress with
tuing PIDs in all 3 axis. Noticed it behaves better with lower G gain. Try to
increase a little next time though. Harder servos (MG 996R) were less precise
and since the heading offset was probably not caused by slack it's better to
stick to softer but more precise Sanwa SRM-102 servos. Maybe consider 2:1 gear
box for more precision. Try flying in the park (for that check if you can
flash the firmware with the Pinebook). Has to respond to input much more.

24.04.20 20:19 - Try tuning pitch and roll first, yaw last. When either pitch
or roll go over a certain treshold yaw correction misbehaves as the plane goes
in to transition state, meaning heading is no longer a rotation around
airframes x axis, it's z instead.

26.04.20 21:38 - Adjust Kalman gains, looks like it needs (much) more filtering.
As of now Q = 0.0001, try less than that next time.

27.04.20 12:30 - First reasonable 5s long tethered flight, Q = 0.00001.
Removed PID tuning on the go. Have some fairly useful data. Improved performance
with Q = 0.000001. Collect throttle data only when throttle is grater than 1500
or hover throttle value. Is it resonable to take off at high angle of attack to
avoid hover? This requires field testing and controling yaw rates instead of
angles. For now master hovering, later consider all horizontal mode at high AOA.

28.04.20 11:40 - Moved the battery to the back for better mass distribution.
Reduced Q to 0.0000001f, tried more dumping in pitch, but it didn't work. Start
flying with no tether - build landing legs!

29.04.20 11:55 - Reduced Q to 0.00000001 (1.0e-8), but that looked worse. Before
"unhooked", fly with prop guards. Guards make more harm then good.

30.04.20 11:45 - Landing legs are in place. New idea - tail suspension for
testing horizontal flight. Imitate lift force with a tail tether. For this to
work need a frontal landing leg. How about a servo pitch offset for high AOA?
Probably not the best idea. How about takeoff sequence? Lift servos up to
maximum, controll pitch with throttle, and roll with differential throttle.
On reaching desired angle transition to horisiontal flight. This means there
is no need to change pitch P gain, probably works simillar to the offset idea.
Increase servosignal frequency to 200Hz! Consider using Madgwicks AHRS algorithm
instead of EKF for shorter loop time. Tune PID in horizontal mode!

01.05.20 13:40 - Deal with vibrations! Lower Q looks better on plots 1.0e-8.
How about softer dumpeners? Soft dumpeners drastically improve the situation!
Leaving Q at 1.0e-7. The frame is too soft. Build printed body with a embedded
carbon spar! Horizontal PID gains are alright, and since vibration levels are
under controll try tuning in vertical hover (are those gains different?).

02.05.20 17:15 - Progress! It's flying tethered with Q = 1.0e-8. Check if roll
angle is correct at high rpm! Looks like there is a small drift, maybe the
sensor board is missaligned. Reducing Q to 1.0e-9 but see no improvement so
reverting back to 1.0e-8. Also, make setpoint 0.1 precision, for some reason
this is not working?

03.05.20 20:30 - Bought L shaped gearbox and ball bearings for new tilt
mechanism. Start drawing the new airframe - two carbon spars and printed shell
with moun holles on flat bottom surface. Try printing with PETG. Created build
and upload shell scripts.

11.05.20 15:30 - With new gimbals in place the airframe is slightly heavier
(720g) but I should have more thurst. I decided not to use the L gearbox as it
was not necessary and it would only add to total weight. With 6" props it should
also be more efficient and quieter. Increased pitch P gain in horiziontal mode
first to 1800 which looked promissing then to 1900 that looked alright as well.
There are signifficant oscilations caused by the leash, real tests should be
done outdoors! Data is saved in the folder named with todays date. Is this
decoupled model good enough for the system?

12.05.20 12:45 - 6" propellers look promising. Motors don't overheat and are
less noisy. There is more thurst. Oscilations are weaker. Radio input is now
coming in as floats, the mapping function had to force a floating point division.
With that fixed the data looks much better. Think about pitch tuning in house
methods. Try hover again with a carbon rod protecting the leash from getting
into spinning propellers!

15.05.20 13:00 - Swaped yaw and roll pid gains for vertical mode. Yaw estimation
gives integers? This is because of wrong formatting with sprintf. I was
accidentally printing a whitespace character after float. Now that's fixed but
compiler is assuming 8 byte float size when using "%f". I've set 6 bytes for
float with "%.2f" precision, I also moved writing whitespace to float printing
function for better readability.

16.05.20 15:10 - Vertical hover looks alright, I've set Q = 0.000001. Flying on
a leash with a long carbon tube works ok, but it's time to fly unleashed! What
is the cause of controller hungups? Can I make the jump in vertical mode?

18.05.20 15:20 - First unteathered takeoff! First serious crash! There is very
large pitch overshoot! Reduce integral gains and rebuild the frame, this time
with carbon tubes in fuselage! Instead of reducing integral gain think about
windup prevention!

20.05.20 12:40 - New frame is much heavier (950g total). Go back to MDF plate.
Taking off from the tail works. After more tuning there still is little
overshoot and some oscilations. Implemented integral anti windup scheme.
Sometimes controller goes crazy after reboot. Is that EKF not converging? The
yaw stick on the controller is imprecise and causes drifts. Slow swing hop
works, tuning is good enough, but there is still room for improvement.
Collected good data!

### Tutorials start

29.06.20 10:00 CEST (meeting with professor Hailes) - Set clear project targets!
Think about board expansions - single board computer with camera (Raspbery Pi,
PC/104 or ESP32 Camera dev boards). Place test points for power and signal
traces (SPI, I2C ...). SPI between F4 and H7. Magnetometer should be calibrated
before flying. Implement calibration algorithms! Look at application notes:
AN4246 - AN4248.

11.07.20 17.33 CEST - Tune PIDs on rates in vertical mode yaw. Tune PID on 
vertical pitch!

13.07.20 12:50 CEST - Tune with wings! New PCB order confirmed!

15.07.20 16:15 CEST - Find out where the gyro noise comes from! Implement matrix
square root for magnetometer calibration! Model yaw-roll coupling! Write
magnetometer data collection routine! Try softer bobbins!

16.07.20 16:15 CEST - There is no gyro noise. The oscilations are caused by control.
Need better roll and yaw tuning!

22.07.20 19:00 CEST - Enabled independent watchdog - reset after 2s without a kick.
RC signal loss failsafe - set motor speed to 1650us for slow descent and pitch
forward a little. Descent for 30s or untill error term on pitch axis is greater
then 20? Then stop the motors and disarm. Glide to safety is an interesting feature
but outiside of the thesis scope. Model yaw + roll coupling. Two SIGLOSS states - 
when disarmed stay disarmed, when flying do safe descent, then disarm.

24.07.20 13:40 CEST - Failsafe signal coming from FS-iA6B (channel 5 == 2000).