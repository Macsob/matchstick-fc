\chapter{Evaluation and testing}
\label{chap:5}
MAV performance evaluation is based on flight data stored in the form of a coma separated values file saved on the blackbox SD card. In flight, on every third pass of the main loop, the flight controller logs aircraft attitude in the form of Euler angles as well as set points commanded by the pilot. UART transmit buffer overflow is the reason for sending data less often. When testing specific software components in isolation, to verify if they work properly, the datalogger was programmed to transmit required information, such as decoded radio controller input or raw AHRS data together with attitude angle estimates or the yaw rate response test presented in appendix \ref{data:yaw_rate_response}. Most test flights were filmed. Selected videos are linked in the README.md file in the project git repository as well as in the corresponding sections below.

\section{Take-off}
The proposed MAV is capable of performing three types of take-off manoeuvre: vertical, short (jump) and hand launch. Flight data and demonstration videos are presented and analysed in the following subsections.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/pic_vertical_takeoff.png}
  \caption{Tailsitter configuration vertical take-off}
  \label{fig:tailsitter}
\end{figure}

\subsection{Vertical}
To take-off vertically the airframe needs to be reconfigured. Landing legs were mounted at  the tail of the vehicle. In the tailsitter configuration the MAV can launch into the air vertically. Take-off data plot \ref{fig:data_tailsitter} demonstrates that the craft pitch angle is set close to 90 degrees from the beginning of the flight. The yaw rate trace follows its setpoint. What's more, the disturbances introduced by to the system as well as pilot commands do not provoke instability. The take-off sequence in tailsitter configuration is presented in figure \ref{fig:tailsitter}. Stills were extracted from a \hyperref{https://drive.google.com/file/d/1mCMTzo7cNs-8hb4SsQD5Ldkj1uyTfOfq/view?usp=sharing}{}{}{flight test video}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\linewidth]{images/data_tailsitter.png}
  \caption{Vertical take-off flight data plot}
  \label{fig:data_tailsitter}
\end{figure}

\subsection{Short}
Short take-off is rather similar to vertical take-off, except that before performing the manoeuvre MAV is positioned horizontally, then the FC increases the pitch angle enough to launch into the air in a almost vertical jump. The pitch and yaw setpoints as well as MAV response during take-off is shown in figure \ref{fig:short_takeoff_pitch_roll}. Complete flight data from start to landing is presented in figure \ref{fig:complete_flight_pitch_roll}. Figure \ref{fig:jump_sequence} is a set of frames cut out from \hyperref{https://drive.google.com/file/d/1seXoBcAygcNN-fPNPAmxdWp57CbBonLq/view?usp=sharing}{}{}{flight test video} which demonstrates that the short take-off sequence works as expected. Just before reaching the desired 90 degree setpoint the pilot commands a gentle decrease of the pitch sepoint value, this helps smooth out the jump. As of now, it requires good flying skills from the MAV operator, but the manoeuvre can be automated. Firstly, the MAV should pitch up to a desired angle of attack, then increase the throttle in order to jump into the air.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/jump_sequence.png}
  \caption{Jump sequence}
  \label{fig:jump_sequence}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.9\linewidth]{images/short_takeoff_pitch_roll_cropped.png}
  \caption{Short take-off pitch and roll data}
  \label{fig:short_takeoff_pitch_roll}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.9\linewidth]{images/complete_flight_pitch_roll.png}
  \caption{Complete jump pitch and roll data}
  \label{fig:complete_flight_pitch_roll}
\end{figure}

\subsection{Hand launch}
Launching the vehicle by hand is quite trivial. Simply increase the throttle slowly and release the vehicle. This was the preferable take-off style for controller response tests since it works regardless of the topography of the terrain. Suspending the drone on a leash was used for initial empirical PID tuning process, however it did not work for testing airframe manoeuvrability. Data plot in figure \ref{fig:hand_launch_data} demonstrates how the PID control loop responds to commanded setpoints. Pitch, roll and yaw rate measurement traces come close to the commanded values. Flight test video is available at \hyperref{https://drive.google.com/file/d/1Rxpy0g-UwTOsMtYD6w80St-gYb4Fj6g6/view?usp=sharing}{}{}{this link}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/pic_hand_launch.png}
  \caption{Hand launch}
  \label{fig:hand_launch}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\linewidth]{images/data_hand_launch_hover.png}
  \caption{Hand launch data plot fragment}
  \label{fig:hand_launch_data}
\end{figure}

\section{Hover}
\hyperref{https://drive.google.com/file/d/1efL-CmwIu3sDwDVAXxk1Q0JTDOygZQxZ/view?usp=sharing}{}{}{Attached video} shows that the MAV is controllable in hover. It responds correctly to the pilot commands and deals well with little disturbances caused by wind. The airframe should be more stable in horizontal flight due to aerodynamic forces acting on the vehicle body. Vertical position is only maintained during the take-off phase, immediately after which the craft should pitch down and transition to horizontal flight. 

Horizontal flight testing is outside of the scope of this project, however, during one of the fail-safe response tests the pitch angle was set too low and the vehicle transitioned into high angle of attack horizontal flight for a short period of time. \hyperref{https://drive.google.com/file/d/1phLdwAVmtqupqIwbEQchr57mfwkWqWI1/view?usp=sharing}{}{}{Linked video} gives an insight into how horizontal flight mode might potentially look like.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\linewidth]{images/pic_hover_01.png}
  \caption{MAV in hover}
  \label{fig:mav_hover}
\end{figure}

\break
\section{Landing}
Landing is the most difficult manoeuvre performed by the pilot because the ground effect introduces a significant amount of disturbance into the system. The steady state error in the pitch trace on plot \ref{fig:mav_landing} is caused by the wing drag. When the throttle is decreased, the control mechanisms authority is reduced and it might not reach the desired setpoint. As soon as the MAV touches the ground the angle of attack is decreased until it reaches horizontal position.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/data_landing.png}
  \caption{MAV landing}
  \label{fig:mav_landing}
\end{figure}

\break
\section{Safe descend}
During safe descend the desired attitude angles are set as 90 degrees pitch, 0 degrees roll and 0 deg/s yaw rate. Looking at plot \ref{fig:data_failsafe} one can come to a conclusion that the MAV behaves as expected. The pitch angle is almost constant until the touchdown moment when it decreases to about 20 degrees - the parking position of the vehicle. It is possible that the MAV swings backwards the moment it touches down just like in the fail-safe \hyperref{https://drive.google.com/file/d/1Mp5rZwJR7Wv8ThY0QvDb8ruRi7mRCO-U/view?usp=sharing}{}{}{test video}. When that happens the FC disarms the system just like in the event of nominal landing. The yaw rate instability is caused by the aerodynamic ground effect. Turbulent air pushed down by the propellers has no escape route and makes the airframe wiggle.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/data_failsafe.png}
  \caption{Fail-safe landing}
  \label{fig:data_failsafe}
\end{figure}