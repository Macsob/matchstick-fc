\chapter{FC Firmware}
\label{chap:4}
The objective of this chapter is to introduce the design and implementation of flight control firmware. Software architecture, see figure \ref{fig:software_arch} is divided into three components - \textit{Drivers}, \textit{FC} and \textit{Storage}. The \textbf{RC Input} block receives data from the radio controller and stores it in a buffer, \textbf{AHRS} block is responsible for reading raw IMU and magnetometer data. Raw data is processed in the \textbf{Sensor Hub} and fed into the \textbf{Attitude \& Rate Controller} that generates output signals to drive the actuators. \textbf{State Machine} is the systems commander, it directs what actions to take based on operator input or external effects. Flight data is logged in the \textbf{BLACKBOX} and saved onto a SD card. The control loop runs at 500Hz. Project code repository can be found at \hyperref{https://gitlab.com/Macsob/matchstick-fc}{}{}{https://gitlab.com/Macsob/matchstick-fc}.

\begin{figure}[!h]
\centering
\includegraphics[scale=.6]{images/software_arch.png}
\caption{Software architecture}{}
\label{fig:software_arch}  
\end{figure}

\section{Low Level Drivers}
As mentioned in \hyperref[chap:3]{Chapter 3} STM32CubeMX was used to generate drivers for the STM32F3 MCU. HAL driver layer provides a set of APIs (application programming interfaces) to interact with the application, libraries and stacks. \textit{"It lets the programmer implement custom functions without knowing in-depth how to use the MCU. As an example, the communication peripherals contain APIs to initialize and configure the peripheral, to manage data transfers based on polling, to handle interrupts or DMA, and to manage communication errors"} \cite{halUserManual}.

\subsection{RC Input}
A generic 2.4GHz radio transmitter can be used for sending commands to the FC. Incoming data is encoded and transmitted over UART peripheral to the FC. For that reason data packets are stored in a ring buffer and decoded on every time the MCU loops. FlySky fs-i6 radio system, used in this project sends 14 integers with values ranging from 1000 to 2000 encoded in IBUS protocol. Serial UART data is transmitted at a standard 115200 baud rate, there is one start bit and 8 data bits. Radio transmitter sends a message every 7 milliseconds. The data packet starts with 0x20, followed by 0x40. Next are 14 pairs of bytes, which is the channel value in little endian byte order. The packet ends with a 2 byte checksum. The checksum is calculated by subtracting the sum of every byte's value from 0xFFFF excluding checksum bytes. MCU uses interrupts to handle both incoming and outgoing serial data. IBUS decoding function as well as the ring buffer implementation can be found in the "ringbuffer.c" file in the project \hyperref{https://gitlab.com/Macsob/matchstick-fc}{}{}{git repository}.

\vspace{5mm}
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\linewidth]{images/ring_buffer.png}
  \caption{Linear ring buffer implementation diagram}
  \label{fig:ring_buff}
\end{figure}

\subsection{Ring buffer}
Ring buffer is a first-in first-out fixed size data structure, a circular software queue. Buffer implemented in the project has two indices to elements within the buffer as shown in figure \ref{fig:ring_buff} where n is the buffer size. Data is put at the head index and read from the tail index. Two ring buffer structs, \textit{rx\_buffer} and \textit{tx\_buffer} are declared in the "ringbuffer.c" file in the project \hyperref{https://gitlab.com/Macsob/matchstick-fc}{}{}{git repository}. The former is used for storing incoming serial UART data, the latter for storing outgoing data. Both storing and sending UART data is handled by interrupts. 

% Functions that read and write to the buffers are presented in listing \ref{lst:ring_buff}. 

A buffer overflow can happen when the buffer is full and there is still incoming data. In such scenario one can either drop the latest data or overwrite the oldest data. In case of FC design it's reasonable to keep the latest data as it might be relevant information in case of vehicle crash.

% \vspace{5mm}
% \begin{lstlisting}[language=C, label={lst:ring_buff}, caption=Ring buffer functions]
% void store_char(unsigned char c, ring_buffer *buffer) {
%   int i = (unsigned int)(buffer->head + 1) % UART_BUFFER_SIZE;

%   if (i != buffer->tail) {
%     buffer->buffer[buffer->head] = c;
%     buffer->head = i;
%   }
% }

% int Uart_read(void) {
%   //  check if the head isn't ahead of the tail
%   if (_rx_buffer->head == _rx_buffer->tail) {  
%     return -1;
%   } else {
%     // read from buffer
%     unsigned char c = _rx_buffer->buffer[_rx_buffer->tail]; 
%     // increment tail
%     _rx_buffer->tail = (unsigned int)(_rx_buffer->tail + 1) % UART_BUFFER_SIZE;
%     return c;
%   }
% }

% void Uart_write(char c) {

%   int i = (_tx_buffer->head + 1) % UART_BUFFER_SIZE;

%   _tx_buffer->buffer[_tx_buffer->head] = (char)c;
%   _tx_buffer->head = i;

%   // Enable UART transmission interrupt
%   __HAL_UART_ENABLE_IT(uart2, UART_IT_TXE);  
% }
% \end{lstlisting}


\subsection{AHRS}
GY-86 sensor module consists of a Invensense MPU-6050 IMU, HMC5883L magnetometer and a MS5611 air pressure sensor. The FC communicates with all three devices using I2C serial bus in \textit{Fast Mode} that defines transfer rates up to 400 kbit/s. To relieve the MCU from the sensor reading task the DMA (Direct Memory Access) module takes care of the data transfer, by coping data from peripheral to memory. Using DMA shortens the operation time to 500 microseconds. It's a significant improvement compared to polling which takes almost 3 milliseconds.

To make this work, HMC5883L magnetometer data needs to be copied to MPU6050 registers. The MPU6050 device combines a 3-axis gyroscope and a 3-axis accelerometer on the same silicon die. It can also access external sensors through an auxiliary master I2C bus. This feature allows for reading full 9dof set of sensor data without an intervention from the MCU. Device registers were configured as instructed in \cite{MPU_Register_Map}. MPU repeats auxiliary I2C data Magnetometer data read at the device sample rate - 400kHz. Magnetometer and IMU data is read by the DMA simultaneously. This is optimal and cannot get any faster. To read and write to auxiliary device registers one needs to write a request in the MPU registers to access the slave device.

C code that reads sensor data lives in the "attitude.c" file. To configure MPU-6050 multiple bits have to be written to the devices 8-bit registers. Since HAL only supports writing 1byte to a specified memory address a custom function was implemented to enable writing selected bits in a device register, keeping the rest unchanged.

% \vspace{5mm}
% \begin{lstlisting}[language=C, label={lst:bit_mask}, caption=Write multiple bits in a 8-bit device register]
% HAL_StatusTypeDef writeBits(uint8_t dev_addr, uint8_t reg_addr, uint8_t start_bit, uint8_t len, uint8_t data) {
% /*     010 value to write
%   76543210 bit numbers
%      xxx   args: bitStart=4, length=3
%   00011100 mask byte
%   10101111 original value (sample)
%   10100011 original & ~mask
%   10101011 masked | value */

%   uint8_t b;
%   HAL_StatusTypeDef err;

%   if ((err = HAL_I2C_Mem_Read(&hi2c1, dev_addr, reg_addr, 1, &b, 1, 1000)) == 0) {
%     uint8_t mask = ((1 << len) - 1) << (start_bit - len + 1);
%     data <<= (start_bit - len + 1);  // shift data into correct position
%     data &= mask;                    // zero all non-important bits in data
%     b &= ~(mask);                    // zero all important bits in existing byte
%     b |= data;                       // combine data with existing byte

%     return HAL_I2C_Mem_Write(&hi2c1, dev_addr, reg_addr, 1, &b, 1, 1000);
%   } else {
%     return err;
%   }
% }
% \end{lstlisting}

\section{Attitude Estimator}
Accurate state estimates are crucial for proper functioning of the flight controller software. The PID controller assumes that yaw, pitch and roll angles are available for feedback. However, sensors that directly measure attitude angles are not available. They have to be estimated from gyroscope, accelerometer and magnetometer sensor data fused together.

\subsection{EKF}
One of the techniques for estimating the state of a MAV is the Kalman Filter. Applications of the EKF in robotics are described in \cite{ThrunSebastian2005Pr/S}. Kalman filtering, also called \textit{linear quadratic estimation}, is an algorithm that uses a series of measurements observed over time to produce estimates of unknown state variables, that tend to be more accurate then those based on a single measurement alone.

As explained in \cite{LeighJ.R.JamesR.2004Ct/J} \textit{"we assume that at time $t=0$, the state $x$ is exactly known, with value $x_0$. We have a process model, that given $x_0$, can make a model based prediction  $T$ seconds into the future, to yield the prediction $x_p(T)$. We also have a measurement $y$ and a know relation $x_m = \alpha y$, applying at all times. In particular we have $x_m(T) = \alpha y(T)$. Both the model used for prediction and the measurement $y$ are assumed to be subject to errors. Thus we have, at time $T$ two estimates of the true state $x(T)$. These are: $x_p(T)$, predicted by a model, $x_m(T)$, based on a measurement. The best estimate of $x(T)$ is denoted $\hat{x}(T)$ and is determined by the relation}

$$\hat{x}(T) = \theta x_p(T) + (1 - \theta) x_m(T)$$

\noindent \textit{Where $\theta$ is a coefficient between 0 and 1 whose value is determined by the relative statistical confidence that can be placed in the accuracy of the model and of the measurement."} The Kalman Filter implementation is based on \cite{Pettersson2015ExtendedKF}.

\begin{algorithm}[!ht]
\SetAlgoLined
\caption{Extended Kalman Filter}

\vspace{5mm}
Predict: \\
\begin{align}
  \hat{\textbf{x}}_k^- &= \textbf{A} \hat{\textbf{x}}_{k-1} + \textbf{B}u_k \label{eq:ekf_1} \\
  \textbf{P}_k^- &= \textbf{A}\textbf{P}_{k-1}\textbf{A}^T + \textbf{Q} \label{eq:ekf_2}
\end{align}

Where:\\
$\hat{\textbf{x}}_k^-$ is the prior state estimate at time step $k$ \\
$\textbf{P}_k^-$ is the prior estimate of the model error at time step $k$\\
$\textbf{Q}$ is the process covariance\\
\vspace{5mm}

Update: \\
\begin{align}
  \textbf{K}_k &= \frac{\textbf{P}_k^- \textbf{C}^T}{\textbf{C}\textbf{P}_k^- \textbf{C}^T + \textbf{R}} \label{eq:ekf_3}\\
  \hat{\textbf{x}}_k &= \hat{\textbf{x}}_k^- + \textbf{K}_k(\textbf{y}_k - \textbf{C}\hat{\textbf{x}}_k^-) \label{eq:ekf_4}\\
  \textbf{P}_k &= (\textbf{I} - \textbf{K}_k\textbf{C})\textbf{P}_k^- \label{eq:ekf_5}
\end{align}

Where:\\
$\textbf{y}_k$ is the sensor measurement vector\\
$\hat{\textbf{x}}_k$ is the posterior estimate of $\textbf{x}$\\
$\textbf{P}_k$ is the posterior estimate of the model error\\
$\textbf{K}_k$ is the filter gain\\
$\textbf{R}$ is the measurement covariance
\vspace{5mm}

\label{algo:ekf}
\end{algorithm}

Since the aircraft states are non-linear the Kalman Filter has to be extended into an Extended Kalman Filter. It works the same as the KF with the exception that non-linearities have to be linearised. As stated earlier using the gyroscope data alone is insufficient because of its bias term. The values drift away from the starting point over time. To remedy this problem, acceleration readings can be used to provide a reference vector pointing towards the center of the Earth and magnetometer data can be used to compute another reference vector pointing towards Earth's magnetic north. Using the two reference vectors, gyro bias can be computed and removed from the attitude estimates. For that reason, states used for the EKF implementation are as follows:

\begin{equation}
\label{eq:x_hat}
    \textbf{x} = \begin{bmatrix}
           q_0 \\
           q_1 \\
           q_2 \\
           q_3 \\
           b_1 \\
           b_2 \\
           b_3 \\
         \end{bmatrix}
\end{equation}

\noindent Where $q_x$ is the quaternion term and $b_x$ is the gyro bias term. Quaternions provide an alternative way of representing attitude. A discussion on the topic of quaternions and rotation sequences is given in \cite{KuipersJackB.1999Qars}. Quaternions offer certain advantages over the Euler angle representation. During take-off and hover the aircraft can encounter orientations which approach gimbal-lock. When pitch angle is 90 degrees, the yaw and roll angles are indistinguishable. As explained in \cite{BeardRandalW2012SuaT}, the attitude kinematics specified by equation \ref{eq:3} cannot be determined since $cos \theta = 0$ when $\theta = 90 \; degrees$. In he quaternion representation this singularity doesn't exist. To use quaternions, equation \ref{eq:3} can be reformulated as:

\begin{equation}
\begin{pmatrix}
\dot{q_0}\\
\dot{q_1}\\
\dot{q_2}\\
\dot{q_3}
\end{pmatrix}=\frac{1}{2}\begin{pmatrix}
 0 & -p & -q & -r  \\
 p &  0 &  r & -q  \\
 q & -r &  0 &  p   \\
 r &  q & -p &  0
\end{pmatrix}\begin{pmatrix}
q_0\\ q_1\\ q_2 \\q_3
\end{pmatrix}
\label{eq:quat_3}
\end{equation}

\vspace{3mm}
\noindent This can be written in short as:

\begin{align}
  \dot{\textbf{q}} &= \frac{1}{2} \textbf{S}(w) \textbf{q} \nonumber \\
  & = \frac{1}{2} \textbf{S}(q) \textbf{w}
\end{align}

\noindent Adding the bias term gives:

\begin{align}
  \dot{\textbf{q}} &= \frac{1}{2} \textbf{S}(w - b) \textbf{q} \nonumber \\
  & = \frac{1}{2} \textbf{S}(w) \textbf{q} - \frac{1}{2} \textbf{S}(b) \textbf{q}
  \label{eq:quat_bias}
\end{align}

\noindent To discretize this equation a first order linearised model is used:

\begin{equation}
  \textbf{q}(k+1) = T \dot{\textbf{q}}(k) + \textbf{q}(k)
  \label{eq:quat_lin}
\end{equation}

\noindent where $T$ is the time elapsed between sample $k$ and $k+1$. After substituting equation \ref{eq:quat_bias} into equation \ref{eq:quat_lin} we get:

\begin{align}
  \textbf{q}(k+1) &= \frac{T}{2} \textbf{S}(w) \textbf{q}(k) - \frac{T}{2} \textbf{S}(b)\textbf{q}(k) + \textbf{q}(k) \nonumber \\
  & = \frac{T}{2} \textbf{S}(q(k)) \textbf{w} - \frac{T}{2} \textbf{S}(q(k))\textbf{b} + \textbf{q}(k)
\end{align}

\noindent and in matrix form:

\begin{equation}
\begin{pmatrix}
\textbf{q}\\
\textbf{b}
\end{pmatrix}_{k+1}=\begin{pmatrix}
 \textbf{I}_{4 \times 4} & - \frac{T}{2}\textbf{S}(q)  \\
 \textbf{0}_{3 \times 4} &  \textbf{I}_{3 \times 3} 
\end{pmatrix}_{k}\begin{pmatrix}
\textbf{q}\\ \textbf{b}
\end{pmatrix}_{k} + \begin{pmatrix}
  \frac{T}{2} \textbf{S}(q) \\
  \textbf{0}_{3 \times 3}
\end{pmatrix}_{k}
\textbf{w}_k
\label{eq:quat_model}
\end{equation}

\vspace{3mm}
\noindent It is equivalent to equation \ref{eq:ekf_1} in the \hyperref[algo:ekf]{EKF algorithm} which attempts to predict the state of the system using its model. Once $\hat{\textbf{x}}_k^-$ is calculated the state quaternion has to be normalized.

Equation \ref{eq:ekf_2} describes the inaccuracies in the model such as inaccuracies of chosen constants or externally applied forces. Values of $\textbf{P}$ and $\textbf{Q}$ matrices have to be chosen when the algorithm is initialised, this process is called filter tuning. Smaller $\textbf{Q}$ means less trust in the data, hence more filtering. In equation \ref{eq:ekf_3}, $\textbf{C}$ matrix converts the state vector into the measurement vector. This has to be done because the reference vector variables are the direction of gravity and magnetic North while the Kalman filter states are the quaternion and gyro bias. The acceleration and magnetometer prediction vector look like so:

\begin{align}
\hat{\textbf{y}}_k^{\-} &= \textbf{C} \hat{\textbf{x}}_k^{\-} \nonumber \\
\begin{pmatrix}
\hat{\textbf{a}}_m^{-}\\
\hat{\textbf{m}}_m^{-}
\end{pmatrix}_{k} &= \begin{pmatrix}
 \textbf{C}_{a} & \textbf{0}_{3 \times 3}  \\
 \textbf{C}_{m} & \textbf{0}_{3 \times 3} 
\end{pmatrix}\begin{pmatrix}
\textbf{q}\\ \textbf{b}
\end{pmatrix}_k
\label{eq:pred_accel_mag}
\end{align}

\noindent Where:

\begin{equation}
  \textbf{C}_a = -2 \begin{bmatrix}
    -q_2 & q_3 & -q_0 & q_1 \\
    q_1  & q_0 & q_3  & q_2 \\
    q_0  & -q_1& -q_2 & q_3
  \end{bmatrix}_{k-1}
\end{equation}


\noindent and:

\begin{equation}
  \textbf{C}_m = -2 \begin{bmatrix}
    q_3 & q_2 & q_1 & q_0 \\
    q_0  & -q_1 & q_2  & -q_3 \\
    -q_1  & -q_0& q_3 & q_2
  \end{bmatrix}_{k-1}
\end{equation}

\vspace{5mm}

To obtain the $\textbf{C}_a$ matrix the non linear quaternion function from the accelerometer data equation \ref{eq:accel} needs to be linearised. This can be done by finding it's gradient at time $k-1$ assuming that the function is linear between two adjacent time steps. The method to obtain $\textbf{C}_m$ matrix is analogous.

\begin{equation}
  {}^{b}\textbf{a}_{m} = {}^{b}\textbf{R}_{w}(-\textbf{g}) + {}^{b}\textbf{e}_{a} + {}^{b}\textbf{b}_{a}
  \label{eq:accel}
\end{equation}

\noindent Where: \\
${}^{b}\textbf{a}_{m}$ is the acceleration measurement in the body frame.\\
${}^{b}\textbf{R}_{w}(-\textbf{g})$ is the inertial to body frame rotation matrix.\\
$\textbf{g}$ is the gravity vector in the inertial frame.\\
${}^{b}\textbf{e}_{a}$ is the accelerometer measurement noise.\\
${}^{b}\textbf{b}_{a}$ is the accelerometer measurement bias.\\


$\textbf{R}$ matrix is the measurement covariance, it tells how accurate the measurement is. The EKF implementation in C can be found in the "EKF.c" file in the project git repository. It uses a matrix operations library included from the "matrix.c" file. Since the STM32F3 FPU deals with floating point operations, solving the EKF equations doesn't consume a significant amount of CPU time. In order to improve the performance the GCC compiler optimisation flag (-O3) was set. The results of the sensor fusion algorithm are presented in figure \ref{plot:fusion_results} with four sub plots: gyroscope, accelerometer, magnetometer and euler angle data. The plots represent three consecutive movements in yaw, pitch and roll axis and demonstrate the workings of the EKF implementation for the STM32F3 microcontroller. To test the sensor fusion accuracy the flight controller was placed on a angle measurement test bench and sequentially rotated to a specific yaw, pitch and roll angle. Live data coming from the UART serial stream was compared with the angle set on the test bench.

\break

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/fusion_gyro_wide.png}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/fusion_accel_wide.png}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/fusion_mag_wide.png}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\linewidth]{images/fusion_euler_wide.png}
  \caption{Sensor fusion results}
  \label{plot:fusion_results}
\end{figure}

% \break
\section{PID Control}
PID controller uses three control terms of proportional, integral and derivative influence on the controller to apply accurate control. In mathematical form the control function looks like so:

\begin{equation}
  u(t) = K_p e(t) + K_i \int_{0}^{t}e(t')dt' + Kd \frac{de(t)}{dt}
  \label{eq:pid}
\end{equation}

The PID controller continuously calculates the error value $e(t)$ as a difference between the setpoint (desired target value) and a measured process variable and applies a correction trying to minimize the error over time. Constants $K_p$ $K_i$ and $K_d$ have to be selected to achieve optimal control. Initial values were chosen intuitively, but they had to be tuned afterwards by observing the system response to control input. Flight data in \hyperref[FlightData]{Appendix A} demonstrates the MAVs response to pilot commands.

\vspace{5mm}
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.7\linewidth]{images/PID.png}
  \caption{PID controller}
  \label{diagram:pid}
\end{figure}
\vspace{3mm}

The MAV can operate in two control modes, acrobatic - the pilot commands rotation rates, and  stabilized - the pilot commands aircraft attitude. The PID controller from "PID.c" file in the project git repository can take both rotation rates or Euler angles as input and output motor command adjustments. The state machine described in section \ref{section:state_machine}, switches controller gains based on system state.

To improve stability by preventing jitter, the controller D term is low pass filtered. The effects of filtering are presented in figures \ref{fig:no_d_term_filter} and \ref{fig:d_term_filter}. On both plots red trace represents the commanded yaw rate value, while the blue trace is the gyroscope yaw axis measurement. Filtering scheme works, yaw rotation rate is less noisy in figure \ref{fig:d_term_filter}.

\begin{figure}[h!]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/without_low_pass.png}
  \caption{In flight yaw rate response\\ without D term filter}
  \label{fig:no_d_term_filter}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/with_low_pass.png}
  \caption{In flight yaw rate response\\ with D term filter}
  \label{fig:d_term_filter}
\end{minipage}
\end{figure}

% For that to work two types of PID loops were implemented. The former takes as input low pass filtered rotation rates and outputs motor command adjustments, the latter takes Euler angle commands and outputs motor command adjustments.

% A nested PID loop was implemented to control the attitude of the MAV. The inner loop controls the rotation rates, while the outers target values are Euler angles as presented in figure X. The controller outputs motor command adjustments. 


Integral wind-up is another common problem with PID control that had to be addressed. When the integral term accumulates a significant error the controller can overshoot and destabilise the system. To prevent this from happening the integral term is zeroed every time the error is zero or changes sign. This situation is detected by multiplying current error with previous error and checking if the product is zero or negative.

% \vspace{5mm}
% \begin{lstlisting}[language=C, label={lst:dterm}, caption=PID correction function with D term low pass filter and anti windup scheme C source]
% float ComputeCorrection(int axis, float cmd, float pos, float dt) {
%   float correction = 0;
%   error[axis] = cmd - pos;

%   /* anti wind up */
%   if (errorPrev[axis] * error[axis] <= 0) {
%     integrator[axis] = 0;
%   } else {
%     integrator[axis] = integrator[axis] + error[axis];
%   }

%   /* D term low pass filter */
%   float dTerm = Kd[axis] * ((error[axis] - errorPrev[axis]) / (dt));
%   float dFiltered = (1 - D_TERM_GAIN) * dTerm +  D_TERM_GAIN * dTermPrev[axis];
%   dTermPrev[axis] = dTerm;

%   /* correction in us */
%   correction = G[axis] * (Kp[axis] * error[axis] + dFiltered +
%                           Ki[axis] * integrator[axis]);

%   errorPrev[axis] = error[axis];

%   return correction;
% }
% \end{lstlisting}

\section{Fail systems}
\label{failsafe}
Safety features are a crucial component of flight control software. Most common failure that can arise and has to be dealt with is the loss of RC signal. The FC was programmed to detect loss of radio signal and respond by setting the pitch angle to 90 degrees and slowly descending until the airframe lands on its tail and tilts either forwards or backwards. Safe descent, in case of motor failure or propeller damage is outside of the scope of the report, however, it's worth further development in future work on the project. The airframe is in theory capable of sustaining lift due to aerodynamic forces in unpowered flight hence it could glide to safety using the gimbals as control surfaces. Glide to safety is a non trivial problem as in contrast to vertical descent the MAV would travel at much grater velocity and might pose threat to infrastructure or people. A more advanced solution is needed to detect a safe landing zone, such as aiming at pre-defined landing area GPS coordinates supported by vision guided navigation.

% \break
\section{State Machine}
\label{section:state_machine}
State machine is the systems commander, it directs what actions to take based on pilot input and external forces acting on the MAV. There are seven defined states the system can transition between given the transition conditions are met. When powered on, FC starts in \textit{Initializing} state and is immediately disarmed! It stays in \textit{Disarmed} unless it's armed using a switch on the radio controller. From there the system cant transition to either \textit{Vertical} or \textit{Horizontal} flight modes in two variants, \textit{Acrobatic} - the pilot controls angular velocities of the MAV and \textit{Stabilised} - pilot commands Euler angles. The operator can always disarm the craft and in case of radio signal loss the state machine transitions to \textit{Failsafe} mode and slowly descends as described in section \ref{failsafe}.

\vspace{5mm}
\begin{figure}[!ht]
\centering
\includegraphics[scale=.6]{images/state_machine.png}
\caption{State Machine}{}
\label{fig:state_machine}  
\end{figure}
