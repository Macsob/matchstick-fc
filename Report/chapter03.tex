\chapter{FC Hardware}
\label{chap:3}
This chapter introduces the flight control hardware. The control algorithms were implemented on a STM32F303K8 microcontroller. To make the assembly process faster, all the components were hand soldered. This simple design, described in more detail in section \ref{section:F3}, was sufficient enough to test the attitude estimation algorithms. Never the less, in preparation for future work on the project, an updated version of the controller was needed. Its design is presented in section \ref{section:F4}. Existing open source flight control solutions could be used, but they would need to be modified to support the proposed vectored thrust propulsion system. A purpose built board allows for more versatility and better understanding of the control mechanisms. \href{https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm}{GNU Arm Embedded Toolchain}, a open-source tool set for C programming, was used to compile bare metal C code for the 32-bit Arm microcontrollers. The microcontroller initialisation code was generated with \href{https://www.st.com/en/development-tools/stm32cubemx.html}{STM32CubeMX} - a graphical tool made for configuration of STM32 devices.

\section{Sensors}
Critical to the functioning of the autopilot are small and lightweight sensors. Based on Micro Electro Mechanical Systems (MEMS) technology, sensors such as gyroscopes, accelerometers and magnetometers provide enough information to estimate vehicles attitude. The technique used for estimating the state of the MAV prototype from sensor measurements is described in \hyperref[chap:4]{Chapter 4}.

\subsection{Gyroscope}
MEMS gyroscopes typically consist of a vibrating proof mass attached to its case with a cantilever beam. Both the proof mass and cantilever are actuated at their resonant frequency to cause oscillation. Rotation around sensors axis results in Coriolis acceleration and a deflection of the cantilever. This deflection can be measured, producing voltage proportional to the lateral Coriolis acceleration. The sensor can be calibrated before each flight by taking the mean of a series of measurements and subtracting it from all coming measurements. MEMS rate gyros are reliable only in the short term. They are susceptible to drift and when integrated over time can produce inaccurate measurements.

\subsection{Accelerometer}
Accelerometers are typically made of a proof mass held in place by a suspension system. When the device experiences an acceleration, the proof mass moves relative to its case through a distance proportional to the acceleration. This displacement can be converted into a voltage output. More detail about the conversion process process can be found in \cite{BeardRandalW2012SuaT}. In vehicle applications accelerometers are mounted near the center of mass, aligned with each of the body axes and measure the specific force in body frame of the vehicle. Accelerometer measurements are subject to signal bias and random uncertainty. The bias term can be calibrated prior to each flight. Aircraft attitude cannot be estimated using accelerometer data only. Instead the gravity vector obtained from accelerometer measurements is used in sensor fusion algorithm discussed in \hyperref[chap:4]{Chapter 4}. Sensor data is reliable only in the long term because, as the vehicle accelerates, the gravity measurement gets disturbed. 

\subsection{Magnetometer}
Magnetometer sensor measures the strength of the magnetic field along the device x, y and z axes. Sensor axis were aligned with the body axes of the airframe. Magnetometers are very sensitive to outside influence. They have to be calibrated after assembly. Magnetic interferences are caused by ferromagnetic elements present in the surroundings of the sensor. Two constant interferences can be compensated for - hard and soft iron. Hard iron results from permanent magnets and remanence of magnetised iron. Soft iron is induced by the interaction of external magnetic field with ferromagnetic materials. Sensor calibration process using least squares ellipsoid fitting \cite{KokM2012Coam} is described in \cite{QingdeLi2004Lses}. It's sufficient to perform the calibration process once after the completion of the MAV build. For the purpose of the project the calibration routine was executed offline, using previously collected sensor data. The results are presented in figures \ref{fig:mag_1} and \ref{fig:mag_2}.

\begin{figure}[h!]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/mag_calib_1.png}
  \caption{Raw magnetometer data}
  \label{fig:mag_1}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/mag_calib_2.png}
  \caption{Ellipsoid after fitting}
  \label{fig:mag_2}
\end{minipage}
\end{figure}

\subsection{Barometer}
Absolute pressure sensors can be used to get some indication of aircraft altitude. This method has certain disadvantages. During take-off or landing ground effect can change the sensor measurement and provide a false reading. It should suffice over small altitude changes where the air density remains almost constant.

\break
\section{STM32F3 Microcontroller Series}
\label{section:F3}
This rather simple brake-out board exposes only few peripherals of the \href{https://www.st.com/en/microcontrollers-microprocessors/stm32f303k8.html}{STM32F303K8} microcontroller. One UART is used for receiving radio controller signals, another dedicated to transmitting flight data to the blackbox. An I2C bus is used for communication with the MEMS sensors - IMU, magnetometer and barometer mounted on a GY-86 brake-out board. A buck converter steps down the battery input voltage and supplies 5V to the servo motors as shown in Figure \ref{fig: STM32F3 diagram}. A detailed schematic is attached in Appendix \ref{schematicsF3}. All components were hand soldered onto the PCB. This approach reduced both the costs and fabrication time at the sacrifice of extra communication lines.

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=.5]{images/hardware-FC_v01.png}
% \caption{STM32F3 controller diagram}{}
% \label{fig: STM32F3 diagram}  
% \end{figure}

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=.1]{images/pcb_2.jpg}
% \caption{STM32F4 controller}{}
% \label{fig:PCB_1}  
% \end{figure}

\vspace{5mm}
\begin{figure}[h!]
\centering
\begin{minipage}{.7\textwidth}
  \centering
  \includegraphics[scale=.4]{images/hardware-FC_v01.png}
  \caption{STM32F3 controller diagram}{}
  \label{fig: STM32F3 diagram}  
\end{minipage}%
\begin{minipage}{.3\textwidth}
  \centering
  \includegraphics[scale=.07]{images/PCB_1.jpg}
  \caption{STM32F4 controller}{}
  \label{fig:PCB_1} 
\end{minipage}
\end{figure}

\section{STM32F4 Microcontroller Series}
\label{section:F4}
The architecture of the extended version of the FC is slightly different. Not only are there many more peripherals exposed but also two F4 series microcontrollers are used. The navigation \href{https://www.st.com/en/microcontrollers-microprocessors/stm32f405-415.html}{STM32F405RGT} is responsible for computing the attitude estimation algorithm while the flight control F4 is supposed to run higher level guidance algorithms and generate PWM signals for motor control. Distributing work between the two computers can  improve computation time, hence allow for faster control loop execution. As shown in Figure \ref{fig: STM32F4 diagram} there is also room for other devices such as current and voltage sensors needed for power consumption analysis.

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=.5]{images/hardware-FC_v10.png}
% \caption{STM32F4 controller diagram}{}
% \label{fig: STM32F4 diagram}  
% \end{figure}

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=.1]{images/pcb_2.jpg}
% \caption{STM32F4 controller}{}
% \label{fig:PCB_2}  
% \end{figure}

\vspace{5mm}
\begin{figure}[h!]
\centering
\begin{minipage}{.7\textwidth}
  \centering
  \includegraphics[scale=.4]{images/hardware-FC_v10.png}
  \caption{STM32F4 controller diagram}{}
  \label{fig: STM32F4 diagram} 
\end{minipage}%
\begin{minipage}{.3\textwidth}
  \centering
  \includegraphics[scale=.07]{images/PCB_2.jpg}
  \caption{STM32F4 controller}{}
  \label{fig:PCB_2}   
\end{minipage}
\end{figure}