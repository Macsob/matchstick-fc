\chapter{Airframe}
\label{chap:2}
This chapter introduces the MAV airframe, backed by research in unconventional aircraft design.

\section{Precedents}

\begin{figure}[h!]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/Vought_V173.jpg}
  \caption{Vought V-173, c. 1942}{\href{https://en.wikipedia.org/wiki/Vought_V-173?oldformat=true}{source url}}
  \label{fig:Vought V-173}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{images/Cornelius_XFG-1.jpg}
  \caption{Cornelius XFG-1}{\href{https://en.wikipedia.org/wiki/Cornelius_XFG-1?oldformat=true}{source url}}
  \label{fig:test2}
\end{minipage}
\end{figure}

The prototype MAV takes some of its design features from existing vehicles. The puller motor configuration needs enough ground clearance to prevent propeller damage, hence the nose of the craft is pointing upwards when stationary. This feature together with STOL capabilities is a derivative of the Vought V-173 "Flying Pancake" first flown in 1942 \cite{FrankGudaitis2005CZah}. Also, worth mentioning here are the Flying Pancake scaled model flight tests. \href{https://youtu.be/LfpTDOAfj7Y?list=WL}{\textit{Attached video}} demonstrates how the plane takes off, hovers and lands. The way V-173 scaled model performs those manoeuvres is very much similar to the intended operation of the proposed MAV and can be compared to the demonstration videos linked in the remaining chapters of the report.

This precedent is rather dated, there has been a lot of development in the aerospace sector since then. In particular, two VTOL capable aircraft configurations are in use nowadays - tailsitter and tiltrotor. The former mostly implemented in small scale UAVs and the latter in large human rated aircraft. However, neither tiltrotor nor tailsitter configurations are on the reports agenda. Instead, a different approach to MAV take-off and landing is introduced.

\section{Design}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/rend01_latex.png}
  \caption{Axes of motion}
  \label{rend:1}
\end{figure}

The MAV shown in figure \ref{rend:1} is positioned horizontally just before take-off, when it swings up to perform an almost vertical jump manoeuvre. Since the craft has to pitch upwards on take-off its wings are slightly swept forward to stop them from touching the ground. The wing configuration is a derivative of the Cornelius XFG-1 glider \cite{GuttmanRobert2006Cavu}. Moderate forward sweep should not introduce significant aerodynamic disadvantages. The aerodynamic analysis of the airframe is outside of the scope of this project. Its design is strictly driven by the mechanical properties that allow for achieving control.

\subsection{Motion Mechanism}
Aerodynamic surfaces such as ailerons, elevator or rudder are not necessary to achieve control in the presence of vectorised thrust mechanism. They were removed from the design as their absence simplifies the fabrication process. Two analog servo motors rotate the gimbals to generate propulsive forces and moments that make attitude control possible.

\begin{figure}[!h]
  \centering
  \begin{minipage}{.5\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/roll_latex.png}
    \caption{Roll}
    \label{rend:roll}
  \end{minipage}%
  \begin{minipage}{.5\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/pitch_latex.png}
    \caption{Pitch}
    \label{rend:pitch}
  \end{minipage}
\end{figure}

\subsection{Dynamic Model}
Building a mathematical model is a crucial process in designing aircraft control mechanisms. The application of mathematical laws aids the analysis of the vehicles behaviour. Model building a sequential formulation of system matrices. \textit{"The process starts with a non-linear aircraft model which is later linearised using small disturbance theory and considering steady state conditions"} \cite{StevensBrianL.2015Acas}.

\begin{table}[!h]
\centering
\begin{tabular}{ |c|l| } 
\hline
Name & Description \\
\hline
$p_n$ & Inertial north position of the MAV measured along $\bf{i}^i$ in $F^i$\\ 
$p_e$ & Inertial east position of the MAV measured along $\bf{j}^i$ in $F^i$ \\
$p_d$ & Inertial down position of the MAV measured along $\bf{k}^i$ in $F^i$\\
$u$   & Body frame velocity measured along $\bf{i}^b$ in $F^b$\\
$v$   & Body frame velocity measured along $\bf{j}^b$ in $F^b$\\
$w$   & Body frame velocity measured along $\bf{k}^b$ in $F^b$\\
$\phi$   & Roll angle defined with respect to $F^{v2}$\\
$\theta$ & Pitch angle defined with respect to $F^{v1}$\\
$\psi$   & Yaw angle defined with respect to $F^{v}$\\
$p$  & Roll rate measured along $\bf{i}^b$ in $F^b$\\
$q$  & Roll rate measured along $\bf{j}^b$ in $F^b$\\
$r$  & Roll rate measured along $\bf{k}^b$ in $F^b$\\
\hline
\end{tabular}
\caption{State variables}
\label{table:statevar}
\end{table}

The first step in developing navigation, guidance and control strategies for MAVs is to introduce systems state variables as in table \ref{table:statevar}. Next, appropriate dynamic models can be developed \cite{BeardRandalW2012SuaT}. This process is described in detail in \cite{BeardRandalW2012SuaT} and \cite{nelson1998flight}. The results are summarised as follows, using shorthand notation $c_x \equiv cos x$, and $s_x \equiv sin x$:

\begin{align}
&
\begin{pmatrix}
\dot{p_n}\\[\jot]
\dot{p_e}\\[\jot]
\dot{p_d}
\end{pmatrix}=\begin{pmatrix}
 c_{\theta}c_{\psi} & s_{\phi}s_{\theta}c_{\psi} - c_{\phi}s_{\psi} & c_{\phi}s_{\theta}c_{\psi} + s_{\phi}s_{\psi} \\
 c_{\theta}s_{\psi} & s_{\phi}s_{\theta}c_{\psi} + c_{\phi}c_{\psi} & c_{\phi}s_{\theta}s_{\psi} - s_{\phi}c_{\psi}\\
 -s_{\theta} & s_{\phi}c_{\theta} & c_{\phi}c_{\theta}
\end{pmatrix}\begin{pmatrix}
u\\[\jot]v\\[\jot]w
\end{pmatrix}
\label{eq:1}
% \end{equation}
\\
% \begin{equation}
&
\begin{pmatrix}
\dot{u}\\[\jot]
\dot{v}\\[\jot]
\dot{w}
\end{pmatrix}=\begin{pmatrix}
 rv - qw \\
 pw - ru \\
 qu - pv
\end{pmatrix}+\frac{1}{M}\begin{pmatrix}
f_x\\ f_y\\ f_z
\end{pmatrix}
\label{eq:2}
% \end{equation}
\\
% \begin{equation}
&
\begin{pmatrix}
\dot{\theta}\\[\jot]
\dot{\phi}\\[\jot]
\dot{\psi}
\end{pmatrix}=\begin{pmatrix}
 1 & sin \phi tan \theta & cos \phi tan \theta  \\
 0 & cos \phi & - sin \phi \\
 0 & \frac{sin \phi}{cos \theta} & \frac{cos \phi}{cos \theta}
\end{pmatrix}\begin{pmatrix}
p\\ q\\ r
\end{pmatrix}
\label{eq:3}
% \end{equation}
\\
% \begin{equation}
&
\begin{pmatrix}
  \dot{p}\\[\jot]
  \dot{q}\\[\jot]
  \dot{r}
\end{pmatrix}=\begin{pmatrix}
  \Gamma_1 pq - \Gamma_2 qr \\
  \Gamma_5 pr - \Gamma_6 (p^2 - r^2) \\
  \Gamma_7 pq - \Gamma_1 qr
\end{pmatrix}+\begin{pmatrix}
  \Gamma_3 l + \Gamma_4 n\\
  \frac{1}{J_y}m\\
  \Gamma_4 l + \Gamma_8 n
\end{pmatrix}
\label{eq:4}
\end{align}

\noindent Where $l, m, n$ are externally applied moments about the $\bf{i}^{b}$, $\bf{j}^b$, $\bf{k}^b$ axis, $J_x, J_y, J_z$ are the moments of inertia, $J_{xz}$ is the product of inertia, $M$ is the vehicle mass and,

\begin{align}
  &\Gamma_1 = \frac{J_{xz}(J_x - J_y + J_z)}{J_x J_z - J_{xz}^2} \nonumber\\
  &\Gamma_2 = \frac{J_{z}(J_z - J_y)+J_{xz}^2}{J_x J_z - J_{xz}^2} \nonumber\\
  &\Gamma_3 = \frac{J_{z}}{J_x J_z - J_{xz}^2} \nonumber\\
  &\Gamma_4 = \frac{J_{xz}}{J_x J_z - J_{xz}^2} \\
  &\Gamma_5 = \frac{J_z - J_x}{J_y} \nonumber\\
  &\Gamma_6 = \frac{J_{xz}}{J_y} \nonumber\\
  &\Gamma_7 = \frac{(J_x - J_y)J_x + J_{xz}^2}{J_x J_z - J_{xz}^2}\nonumber \\
  &\Gamma_8 = \frac{J_x}{J_x J_z - J_{xz}^2} \nonumber
\end{align}

\noindent The general rigid-body equations of motion form the skeleton of the aircraft model, but aerodynamic forces and moments are not yet defined. Four control variables $U_1, U_2, U_3, U_4$, that represent propulsion forces are introduced to the model:

\begin{equation}
\label{eq:5}
\begin{pmatrix}
  U_1\\
  U_2\\
  U_3\\
  U_4\\
\end{pmatrix}=\begin{pmatrix}
  K_T(\omega_1^2 cos\xi_1 + \omega_2^2 cos\xi_2) \\
  K_T(\omega_1^2 cos\xi_1 - \omega_2^2 cos\xi_2) \\
  K_T(\omega_1^2 sin\xi_1 + \omega_2^2 sin\xi_2) \\
  K_T(\omega_1^2 sin\xi_1 - \omega_2^2 sin\xi_2)
\end{pmatrix}
\end{equation}

\noindent Using equations \ref{eq:1} to \ref{eq:4} and the propulsion forces model \ref{eq:5}, the equations of linear and angular motion in the inertial coordinate system can be derived as stated in \cite{QiminZhang2016Maac}.

\begin{align}
  \label{eq:6}
  &\ddot{p_n} = - \frac{sin\phi sin\psi + cos\phi sin\theta cos\psi}{m} K_T(\omega_1^2 cos\xi_1 + \omega_2^2 cos\xi_2) \nonumber \\
  & - \frac{cos\theta cos\psi}{m} K_T(\omega_1^2 sin\xi_1 + \omega_2^2 sin\xi_2) \nonumber \\ 
  &\ddot{p_e} = \frac{sin\phi cos\psi - cos\phi sin\theta cos\psi}{m} K_T(\omega_1^2 cos\xi_1 + \omega_2^2 cos\xi_2) \nonumber \\
  & + \frac{cos\theta sin\psi}{m} K_T(\omega_1^2 sin\xi_1 + \omega_2^2 sin\xi_2) \nonumber \\
  & \ddot{p_d} = - \frac{cos\phi cos\theta}{m} K_T(\omega_1^2 cos\xi_1 + \omega_2^2 cos\xi_2) \\
  & - \frac{sin\theta}{m} K_T(\omega_1^2 sin\xi_1 + \omega_2^2 sin\xi_2) + g \nonumber \\
  & \ddot{\phi} = \frac{1}{J_x[(-J_z + J_y)qr + LK_T(\omega_1^2 cos\xi_1 - \omega_2^2 cos\xi_2)]} \nonumber \\
  & \ddot{\theta} = \frac{1}{J_y[(J_z - J_x)pr + HK_T(\omega_1^2 sin\xi_1 + \omega_2^2 sin\xi_2)]} \nonumber \\
  & \ddot{\psi} = \frac{1}{J_z[(J_x - J_y)pq + LK_T(\omega_1^2 sin\xi_1 - \omega_2^2 sin\xi_2)]} \nonumber
\end{align}
  

\noindent Where $L$ is the distance from the rotor to the center of mass and $H$ is the distance from the rotor axis to the center of mass. This coupled model can be decomposed into a relatively independent control channel. This set of equations can be simplified to:

\begin{align}
  \label{eq:7}
  &\ddot{p_n} = - \frac{sin\phi sin\psi + cos\phi sin\theta cos\psi}{m} U_1 - \frac{cos\theta cos\psi}{m} U_3 \nonumber \\ 
  &\ddot{p_e} = \frac{sin\phi cos\psi - cos\phi sin\theta cos\psi}{m} U_1 + \frac{cos\theta sin\psi}{m} U_3 \nonumber \\
  & \ddot{p_d} = - \frac{cos\phi cos\theta}{m} U_1 - \frac{sin\theta}{m} U_3 + g \\
  & \ddot{\phi} = \frac{L U_2}{J_x} \nonumber \\
  & \ddot{\theta} = \frac{H U_3}{J_y} \nonumber \\
  & \ddot{\psi} = \frac{L U_4}{J_z} \nonumber
\end{align}

\subsection{Yaw Roll Coupling}
Yaw roll coupling is an interesting feature, never the less it is treated as a disturbance of the system and ignored by the PID controller. Probably could be utilised when using a different control approach such as Model Predictive Control or Backstepping control.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.7\linewidth]{images/up01_latex.png}
  \caption{Yaw Roll coupling}
  \label{rend:coupling}
\end{figure}

\section{Prototype}
Designed according to DFM/DFA principles, the prototype airframe presented in figure \ref{photo:airframe} was first modelled in a CAD environment and then constructed using both subtractive and additive rapid fabrication techniques. Rotating canards are 3D printed in PETG, durable and easily accessible polymer. They are mounted on a carbon fibre spar attached to the servo motors. Aeroplane wings were machined on both sides out of a solid block of Polyurethane foam. To add stiffness two carbon fibre spares were fixed along the leading edge. Vectored thrust mechanism, wings, LiPo battery and the controller PCB are attached to a laser cut 5mm MDF board.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\linewidth]{images/airframe.jpg}
  \caption{MAV airframe prototype}
  \label{photo:airframe}
\end{figure}

\section{Requirements}
To realise the aims of the project certain requirements had to be met. Most of all, the need to take off and land vertically, fly horizontally and have safety features like descent on loss of radio controller signal. Although horizontal flight is an important feature of the aircraft this project is only focused on the analysis of vertical flight mode and take-off and landing manoeuvres.

\section{Summary}
The airframe design decisions were only influenced by the mechanical limitations of the VTOL capable miniature aerial vehicle, no aerodynamic properties were taken into consideration. This chapter explained the motivation behind certain design decisions as well as described the dynamic model of the MAV.
