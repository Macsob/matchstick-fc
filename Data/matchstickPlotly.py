import pandas as pd
import plotly.graph_objects as go
import numpy as np
import os

# Get data directory
dirPath = os.path.dirname(os.path.realpath(__file__))
folder = '/CSV/'
fileName = 'ANALOG75.CSV'
path = dirPath + folder + fileName

df = pd.read_csv(path, delim_whitespace=True, names=[
                 'Roll', 'SetRoll', 'Pitch', 'SetPitch', 'Yaw', 'SetYaw', 'Throttle', 'LoopTime'])

df.replace('', np.nan, inplace=True)
df.dropna(axis=0, how='any', inplace=True)
df.reset_index(drop=True, inplace=True)
df.drop(0, inplace=True)

print(df.head())

# Create traces
fig = go.Figure()

fig.add_trace(go.Scatter(y=df.Yaw,
                         mode='lines',
                         name='Yaw Rate'))
fig.add_trace(go.Scatter(y=df.SetYaw,
                         mode='lines',
                         name='Yaw Setpoint'))

fig.add_trace(go.Scatter(y=df.Pitch,
                         mode='lines',
                         name='Pitch'))
fig.add_trace(go.Scatter(y=df.SetPitch,
                         mode='lines',
                         name='Pitch Setpoint'))

fig.add_trace(go.Scatter(y=df.Roll,
                         mode='lines',
                         name='Roll'))
fig.add_trace(go.Scatter(y=df.SetRoll,
                         mode='lines',
                         name='Roll Setpoint'))

fig.add_trace(go.Scatter(y=df.Throttle,
                         mode='lines',
                         name='Throttle'))

fig.add_trace(go.Scatter(y=df.LoopTime,
                         mode='lines',
                         name='Loop Time'))

fig.update_layout(template='plotly_white',
                  title=fileName,
                  xaxis=dict(
                      range=[0, 2000],  # sets the range of xaxis
                      constrain="domain",  # meanwhile compresses the xaxis by decreasing its "domain"
                      rangeslider_visible=True,
                  ),
                  yaxis=dict(
                      range=[-120, 120],
                      constrain="domain",
                  ))
fig.show(renderer='browser')
