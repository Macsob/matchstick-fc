import pandas as pd
import plotly.graph_objects as go
import numpy as np
import os

# Get data directory
dirPath = os.path.dirname(os.path.realpath(__file__))
folder = '/CSV/Warszawa/'
fileName = 'ANALOG27.CSV'
path = dirPath + folder + fileName

df = pd.read_csv(path, delim_whitespace=True, names=[
                 'Gx', 'Gy', 'Gz', 'Ax', 'Ay', 'Az', 'Mx', 'My', 'Mz', 'Roll', 'Pitch', 'Yaw'])

df.replace('', np.nan, inplace=True)
df.dropna(axis=0, how='any', inplace=True)
df.reset_index(drop=True, inplace=True)
df.drop(0, inplace=True)

print(df.head())

# Create traces
fig = go.Figure()

fig.add_trace(go.Scatter(y=df.Gx,
                         mode='lines',
                         name='Gx'))
fig.add_trace(go.Scatter(y=df.Gy,
                         mode='lines',
                         name='Gy'))
fig.add_trace(go.Scatter(y=df.Gz,
                         mode='lines',
                         name='Gz'))

fig.add_trace(go.Scatter(y=df.Ax,
                         mode='lines',
                         name='Ax'))
fig.add_trace(go.Scatter(y=df.Ay,
                         mode='lines',
                         name='Ay'))
fig.add_trace(go.Scatter(y=df.Gz,
                         mode='lines',
                         name='Az'))

fig.add_trace(go.Scatter(y=df.Mx,
                         mode='lines',
                         name='Mx'))
fig.add_trace(go.Scatter(y=df.My,
                         mode='lines',
                         name='My'))
fig.add_trace(go.Scatter(y=df.Mz,
                         mode='lines',
                         name='Mz'))

fig.add_trace(go.Scatter(y=df.Yaw,
                         mode='lines',
                         name='Yaw'))
fig.add_trace(go.Scatter(y=df.Pitch,
                         mode='lines',
                         name='Pitch'))
fig.add_trace(go.Scatter(y=df.Roll,
                         mode='lines',
                         name='Roll'))


fig.update_layout(template='plotly_white',
                  title=fileName,
                  xaxis=dict(
                      range=[0, 2000],  # sets the range of xaxis
                      constrain="domain",  # meanwhile compresses the xaxis by decreasing its "domain"
                      rangeslider_visible=True,
                  ),
                  yaxis=dict(
                      range=[-120, 120],
                      constrain="domain",
                  ))
fig.show(renderer='browser')
