from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
# import pandas as pd


class serialPlot:
  def __init__(self, serialPort='/dev/rfcomm0', serialBaud=115200, plotLength=100, numPlots=1):
    self.port = serialPort
    self.baud = serialBaud
    self.plotMaxLength = plotLength
    self.numPlots = numPlots
    self.line = ""
    self.lineList = []
    self.data = []
    for i in range(numPlots):   # give an array for each type of data and store them in a list
      self.data.append(collections.deque([0] * plotLength, maxlen=plotLength))
    self.isRun = True
    self.isReceiving = False
    self.thread = None
    # self.plotTimer = 0
    # self.previousTimer = 0
    # self.csvData = []

    print('Trying to connect to: ' + str(serialPort) +
          ' at ' + str(serialBaud) + ' BAUD.')
    try:
      self.serialConnection = serial.Serial(
          serialPort, serialBaud, timeout=4)
      print('Connected to ' + str(serialPort) +
            ' at ' + str(serialBaud) + ' BAUD.')
    except:
      print("Failed to connect with " + str(serialPort) +
            ' at ' + str(serialBaud) + ' BAUD.')

  def readSerialStart(self):
    if self.thread == None:
      self.thread = Thread(target=self.backgroundThread)
      self.thread.start()
      # Block till we start receiving values
      while self.isReceiving != True:
        time.sleep(0.1)

  def getSerialData(self, frame, lines, lineValueText, lineLabel, timeText, pltInterval):
    timeText.set_text('Plot Interval = ' + str(pltInterval) + 'ms')

    privData = self.lineList
    for i in range(self.numPlots):
      value = float(privData[i])
      self.data[i].append(value)
      lines[i].set_data(range(self.plotMaxLength), self.data[i])
      lineValueText[i].set_text('[' + lineLabel[i] + '] = ' + str(value))
    # self.csvData.append([self.data[0][-1], self.data[1][-1], self.data[2][-1]])

  def backgroundThread(self):    # retrieve data
    time.sleep(1.0)  # give some buffer time for retrieving data
    self.serialConnection.reset_input_buffer()
    while (self.isRun):
      self.line = self.serialConnection.readline()
      self.lineList = self.line.split(b' ')
      self.isReceiving = True
      # self.csvData.append(self.line)
      # print(self.lineList)

  def close(self):
    self.isRun = False
    self.thread.join()
    self.serialConnection.close()
    print('Disconnected...')
    # df = pd.DataFrame(self.csvData)
    # df.set_index(df.columns[0], inplace=True)
    # df = pd.DataFrame(self.csvData)


def main():
  portName = '/dev/rfcomm0'
  baudRate = 115200
  maxPlotLength = 100     # number of points in x-axis of real time plot
  numPlots = 6            # number of traces in 1 graph
  # initializes all required variables
  s = serialPlot(portName, baudRate, maxPlotLength, numPlots)
  # starts background thread
  s.readSerialStart()

  # plotting starts below
  pltInterval = 5    # Period at which the plot animation updates [ms]
  xmin = 0
  xmax = maxPlotLength
  ymin = -(180)
  ymax = 180
  fig = plt.figure(figsize=(10, 8))
  ax = plt.axes(xlim=(xmin, xmax), ylim=(
      float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))
  ax.set_title('Kalman Filter')
  ax.set_xlabel("Time")
  ax.set_ylabel("Output")

  lineLabel = ['roll', 'setRoll', 'pitch', 'setPitch', 'yaw', 'setYaw']
  # linestyles for the different plots
  style = ['r--', 'c--', 'b-', 'g--', 'y--', 'm-']
  timeText = ax.text(0.50, 0.95, '', transform=ax.transAxes)
  lines = []
  lineValueText = []
  for i in range(numPlots):
    lines.append(ax.plot([], [], style[i], label=lineLabel[i])[0])
    lineValueText.append(
        ax.text(0.50, 0.90-i*0.05, '', transform=ax.transAxes))
  anim = animation.FuncAnimation(fig, s.getSerialData, fargs=(
      lines, lineValueText, lineLabel, timeText, pltInterval), interval=pltInterval)    # fargs has to be a tuple

  plt.legend(loc="upper left")
  plt.show()

  s.close()


if __name__ == '__main__':
  main()
