#ifndef matrix_h
#define matrix_h

// void printMx(int r, int c, float m[c][r]);
void identity(int size, float mx[size][size], float val);
void ones(int r, int c, float mx[r][c], float val);
void scalarMultiply(int r, int c, float mx[c][r], float val);
void mxPow2(int r, int c, float mx[r][c]);
void mxElemMul(int r, int c, float A[r][c], float B[r][c], float C[r][c]);
void slice2D(int r, int c, int startRow, int stopRow, int startCol, int stopCol, float in[r][c], float out[stopRow - startRow][stopCol - startCol]);
void transpose(int r, int c, float in[r][c], float out[c][r]);
void concat_0(int rA, int rB, int c, float A[c][rA], float B[c][rB], float C[c][rA + rB]);
void concat_1(int r, int cA, int cB, float A[cA][r], float B[cB][r], float C[cA + cB][r]);
void mxMul(int rA, int cA, float A[cA][rA], int rB, int cB, float B[cB][rB], float C[cB][rA]);
void mxAdd(int r, int c, float A[c][r], float B[c][r], float C[c][r]);
void mxSub(int r, int c, float A[r][c], float B[r][c]);
void mxAddEq(int r, int c, float A[r][c], float B[r][c]);
void writeOver(int r, int c, float A[r][c], float B[r][c]);
void mxInverse(int size, float mx[size][size], float out[size][size]);
int row_swap(int size, float mx[size][size], int a, int b);
int reduce(int size, float mx[size][size], int a, int b, float factor);
float invSqrt(float x);
void invert(int size, float in[size][size], float out[size][size]);

int inverse(int n, float A[n][n], float inverse[n][n]);
void write1Row(int n, int r, int c, float vec[1][c], float mx[r][c]);
void multiplyRowByVect(int n, int r, int c, float vec[1][c], float mx[r][c]);
void multiplyRowByScalar(int n, int r, int c, float scalar, float mx[r][c]);
void write1RowScalar(int n, int r, int c, float scalar, float mx[r][c]);

#endif /* matrix */