#ifndef PID_H_
#define PID_H_

#define AXIS_NB 9
#define D_TERM_GAIN 0.95f

enum yprPID { rollAxis, pitchAxis, yawAxis, rollSpeedVert, pitchSpeedVert, yawSpeedVert, rollSpeedHor, pitchSpeedHor, yawSpeedHor };

// float rollGains[4];
// float pitchGains[4];
// float yawGains[4];

/* horizontal angle gains */
float rollAngleHorGains[4];
float pitchAngleHorGains[4];
float yawAngleHorGains[4];

/* vertical angle gains */
float rollAngleVertGains[4];
float pitchAngleVertGains[4];
float yawAngleVertGains[4];

float yawSpeedVertGains[4];
float pitchSpeedVertGains[4];
float rollSpeedVertGains[4];

float yawSpeedHorGains[4];

float G[AXIS_NB], Kp[AXIS_NB], Ki[AXIS_NB], Kd[AXIS_NB];
// float speedCmd[3];

float posErrorPrev[AXIS_NB];
float posIntegrator[AXIS_NB];

float error[AXIS_NB];
float errorPrev[AXIS_NB];
float integrator[AXIS_NB];

void SetGains(int axis, float params[4]);
float ComputeCorrection(int axis, float cmd, float pos, float dt);


#endif