#ifndef EKF_H_
#define EKF_H_

float xHat[7][1];
float xHatBar[7][1];
float xHatPrev[7][1];
float yHatBar[6][1];
float p[7][7];
float pBar[7][7];
float Q[7][7];  // process variance
float R[6][6];  // measurement variance
float K[7][6];
float A[7][7];
float B[7][3];
float C[6][7];
float accRef[3][1];
float magRef[3][1];

void getRotMx(float quaterion[7][1], float rotMx[3][3]);
float rad2deg(float rad);
void getEuler();
void initKalman();
void normaliseQuat(int r, float mx[r][1]);
void getAccVec(float a1, float a2, float a3, float a[3][1]);
void getMagVec(float m1, float m2, float m3, float m[3][1]);
void getJacobian(float ref[3][1], float j[3][4]);
void predictAccMag(float AccMag[6][1]);
void predict(float w1, float w2, float w3, float dt);
void update(float a1, float a2, float a3, float m1, float m2, float m3);

#endif