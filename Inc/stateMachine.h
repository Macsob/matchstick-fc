#pragma once

#define constrain(amt, low, high) ((amt) < (low) ? (low) : ((amt) > (high) ? (high) : (amt)))

#define ROLL_CHANNEL 0
#define PITCH_CHANNEL 1
#define THROTTLE_CHANNEL 2
#define YAW_CHANNEL 3
#define SWD_CHANNEL 4
#define ARM_CHANNEL 5
#define SWA_CHANNEL 6
#define STAB_CHANNEL 7
#define VRB_CHANNEL 8
#define VRA_CHANNEL 9

#define LEFT_SERVO_OFFSET 0
#define RIGHT_SERVO_OFFSET 0
#define THROT_OFFSET 50

#define DELTA_T 0.0054f
#define VERTICAL_CONST 98
#define TRANSITION_ANGLE 55
#define PITCH_DESCENT_ANGLE 90
#define DESCENT_THROTTLE 1710
#define PITCH_LANDED_TRASHOLD 20

float setYaw;
extern float loopTime;
extern int mag_calibrated;

float mapFloat(int x, int in_min, int in_max, int out_min, int out_max);
int getFlyingMode();

enum flyingMode { disarmed, vertical, horizontal, failsafe };
enum stabilisation { accro, angle };

typedef void *(*StateFunc)();
StateFunc statefunc;

/* state functions */
void *initState();
void *startingState();
void *angleVertState();
void *angleHorState();
void *accroVertState();
void *accroHorState();
void *safetyState();
void *disarmedState();
void *rclossState();

