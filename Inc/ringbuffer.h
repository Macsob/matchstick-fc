/*
 * ringbuffer.h
 */

#pragma once

#include "stm32f3xx_hal.h"

/* change the size of the buffer */
#define UART_BUFFER_SIZE 512

#define IBUS_FRAME 31
#define IBUS_SYNCBYTE 0x55
#define IBUS_MAXCHANNELS 14
#define NEW_LINE_CHAR 0x0A //0x0D

#define PROTOCOL_LENGTH  0x20
#define PROTOCOL_OVERHEAD 3 // <len><cmd><data....><chkl><chkh>
#define PROTOCOL_TIMEGAP 3 // Packets are received very ~7ms so use ~half that for the gap
#define PROTOCOL_CHANNELS 10
#define PROTOCOL_COMMAND40 0x40 // Command is always 0x40


enum State {
  GET_LENGTH,
  GET_DATA,
  GET_CHKSUML,
  GET_CHKSUMH,
  DISCARD,
};

uint8_t state;
uint32_t last;
uint8_t buffer[PROTOCOL_LENGTH];
uint8_t ptr;
uint8_t len;
uint16_t channel[PROTOCOL_CHANNELS];
uint16_t chksum;
uint8_t lchksum;

/* validate IBUS data */
void IBUS(void);
uint16_t readChannel(uint8_t channelNr);

/* ring buffer struct */
typedef struct {
  unsigned char buffer[UART_BUFFER_SIZE];
  volatile unsigned int head;
  volatile unsigned int tail;
} ring_buffer;

extern uint16_t radioInput[IBUS_MAXCHANNELS];

char sprintf_buf[50];

void store_char(unsigned char c, ring_buffer *buffer);

/* reads the data in the rx_buffer and increment the tail count in rx_buffer */
int Uart_read(void);

/* writes the data to the tx_buffer and increment the head count in tx_buffer */
void Uart_write(char c);

/* prepare data to write to the tx_buffer */
// void Uart_print_int(int c);
// void Uart_print_float(float c);
// void Uart_print_mx(int r, int c, float mx[r][c]);
// void Uart_send_float(float data); // using HAL_UART_Transmit_IT
// void floatToString(char* str, float x, int precision);
void Uart_send(char* s);

/* Initialize the ring buffer */
void Ringbuf_init(void);

/* checks if the data is available to read in the rx_buffer */
// int IsDataAvailable(void);

/* the ISR for the uart. put it in the IRQ handler */
void Uart_isr_rx(UART_HandleTypeDef *huart);
void Uart_isr_tx(UART_HandleTypeDef *huart);

/* validate pseudo IBUS data */
void pseudoIBUS(void);

