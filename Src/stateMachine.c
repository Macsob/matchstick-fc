#include "stateMachine.h"
#include "EKF.h"
#include "ringbuffer.h"
#include "attitude.h"
#include "PID.h"
#include "matrix.h"
#include <stdio.h>

extern float mag_A_inv[3][3];
extern float mag_b[3][1];

int map(int x, int in_min, int in_max, int out_min, int out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/* divident is cast to float for floating point division */
float mapFloat(int x, int in_min, int in_max, int out_min, int out_max) {
  float divident = (x - in_min) * (out_max - out_min);
  return divident / (in_max - in_min) + out_min;
}

int getFlyingMode() {
  if (readChannel(SWD_CHANNEL) > 1500) {
    return failsafe;
  } else
  if (readChannel(ARM_CHANNEL) == 1000)
    return disarmed;
  else if (readChannel(ARM_CHANNEL) == 1500)
    return vertical;
  else if (readChannel(ARM_CHANNEL) == 2000)
    return horizontal;
  else
    return disarmed;
}

int getStabilisation() {
  if (readChannel(STAB_CHANNEL) == 1000)
    return accro;
  else
    return angle;
}

/* reverse servo signal */
int16_t reverse(int16_t val) {
  return (val - 1500) * -1 + 1500;
}
/* add servo signal */
int16_t add(int16_t a, int16_t b) {
  int16_t out = (a - 1500 + b - 1500) + 1500;
  return constrain(out, 1000, 2000);
}

/* callback functions */
void *initState() {
  TIM2->CCR2 = 980;
  TIM2->CCR1 = 980;
  SetGains(rollAxis, rollAngleVertGains);
  SetGains(pitchAxis, pitchAngleVertGains);
  // SetGains(yawAxis, yawAngleVertGains); // not using those
  SetGains(yawSpeedVert, yawSpeedVertGains);
  SetGains(pitchSpeedVert, pitchSpeedVertGains);
  SetGains(rollSpeedVert, rollSpeedVertGains);

  SetGains(yawSpeedHor, yawSpeedHorGains);

  return disarmedState;
}

void *disarmedState() {
  TIM2->CCR2 = 980;
  TIM2->CCR1 = 980;

  /* left servo */
  TIM1->CCR1 = 1500;
  /* right servo */
  TIM1->CCR4 = 1500;

  /* compute gyro offsets */
  if (readChannel(SWA_CHANNEL) > 1500) {
    if (computeOffsets() == 1) {
      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET);
      HAL_Delay(1000);
      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
    }
  }

  /* reset integrals */
  integrator[rollAxis] = 0;
  integrator[pitchAxis] = 0;
  integrator[yawAxis] = 0;

  if (getFlyingMode() == vertical && getStabilisation() == accro)
    return accroVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == accro)
    return accroHorState;
  else if (getFlyingMode() == vertical && getStabilisation() == angle)
    return angleVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == angle)
    return angleHorState;

  return disarmedState;
}

void *accroVertState() {
  /* yaw rate command */
  float setYaw = -mapFloat(readChannel(YAW_CHANNEL), 1000, 2000, -135, 135);
  int16_t yawRateCorr = (int16_t)ComputeCorrection(yawSpeedVert, setYaw, Gy, loopTime);
  int16_t yawRateCmd = constrain(1500 - yawRateCorr, 1000, 2000);

  /* pitch rate command */
  float setPitch = -mapFloat(readChannel(PITCH_CHANNEL), 1000, 2000, -135, 135);
  int16_t pitchRateCorr = (int16_t)ComputeCorrection(pitchSpeedVert, setPitch, Gx, loopTime);
  int16_t pitchRateCmd = constrain(1500 - pitchRateCorr, 1000, 2000); 

  /* roll rate command */
  float setRoll = -mapFloat(readChannel(ROLL_CHANNEL), 1000, 2000, -135, 135);
  int16_t rollRateCorr = (int16_t)ComputeCorrection(rollSpeedVert, setRoll, Gz, loopTime);

  /* left motor mixing*/
  TIM2->CCR2 = constrain(readChannel(THROTTLE_CHANNEL) - rollRateCorr, 1000, 2000) - THROT_OFFSET;
  /* right motor mixing */
  TIM2->CCR1 = constrain(readChannel(THROTTLE_CHANNEL) + rollRateCorr, 1000, 2000) - THROT_OFFSET;

  /* left servo mixing */
  TIM1->CCR1 = add(reverse(yawRateCmd), reverse(pitchRateCmd)) - LEFT_SERVO_OFFSET;
  /* right servo  mixing */
  TIM1->CCR4 = add(reverse(yawRateCmd), pitchRateCmd) - RIGHT_SERVO_OFFSET;

  /* double promotion here! has to be fixed! */
  if (readChannel(THROTTLE_CHANNEL) > 1100) {
    sprintf(sprintf_buf, "%.3f %.3f %.3f %.3f %.3f %.3f %d\n", (double)Gz, (double)setRoll, (double)Gx, (double)setPitch, (double)Gy, (double)setYaw, readChannel(THROTTLE_CHANNEL));
    Uart_send(sprintf_buf);
  }

  /* set state */
  if (getFlyingMode() == failsafe) {
    return rclossState;
  } else
  if (getFlyingMode() == disarmed)
    return disarmedState;
  else if (getFlyingMode() == horizontal && getStabilisation() == accro)
    return accroHorState;
  else if (getFlyingMode() == vertical && getStabilisation() == angle)
    return angleVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == angle)
    return angleHorState;

  return accroVertState;
}

void *accroHorState() {
  int16_t yawDiff = constrain(readChannel(YAW_CHANNEL) - 1500, -80, 80);

  /* left motor */
  TIM2->CCR2 = constrain(readChannel(THROTTLE_CHANNEL) + yawDiff, 1000, 2000) - THROT_OFFSET;
  /* right motor */
  TIM2->CCR1 = constrain(readChannel(THROTTLE_CHANNEL) - yawDiff, 1000, 2000) - THROT_OFFSET;

  /* left servo */
  TIM1->CCR1 = add((readChannel(ROLL_CHANNEL)), reverse(readChannel(PITCH_CHANNEL))) - LEFT_SERVO_OFFSET;
  /* right servo */
  TIM1->CCR4 = add((readChannel(ROLL_CHANNEL)), readChannel(PITCH_CHANNEL)) - RIGHT_SERVO_OFFSET;

  /* set state */
  if (getFlyingMode() == disarmed)
    return disarmedState;
  else if (getFlyingMode() == vertical && getStabilisation() == accro)
    return accroVertState;
  else if (getFlyingMode() == vertical && getStabilisation() == angle)
    return angleVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == angle)
    return angleHorState;

  return accroHorState;
}

void *angleVertState() {
  /* roll position command, swapped with yaw gains! */
  float setRoll = mapFloat(readChannel(ROLL_CHANNEL), 1000, 2000, -20, 20);
  int16_t rollAngleCmd = (int16_t)ComputeCorrection(rollAxis, setRoll, roll, loopTime);

  /* pitch position command */
  float setPitch =
      VERTICAL_CONST + mapFloat(reverse(readChannel(PITCH_CHANNEL)), 1000, 2000, -20, 20);  // this is inverted
  int16_t pitchCorr = (int16_t)ComputeCorrection(pitchAxis, setPitch, pitch, loopTime);
  int16_t pitchAngleCmd = constrain(1500 - pitchCorr, 1000, 2000);

  /* yaw rate command */
  float setYaw = -mapFloat(readChannel(YAW_CHANNEL), 1000, 2000, -135, 135);
  int16_t yawRateCorr = (int16_t)ComputeCorrection(yawSpeedVert, setYaw, Gy, loopTime);
  int16_t yawRateCmd = constrain(1500 - yawRateCorr, 1000, 2000);

  /* left motor mixing*/
  TIM2->CCR2 = constrain(readChannel(THROTTLE_CHANNEL) + rollAngleCmd, 1000, 2000) - THROT_OFFSET;
  /* right motor mixing */
  TIM2->CCR1 = constrain(readChannel(THROTTLE_CHANNEL) - rollAngleCmd, 1000, 2000) - THROT_OFFSET;

  /* left servo mixing */
  TIM1->CCR1 = add(reverse(yawRateCmd), reverse(pitchAngleCmd)) - LEFT_SERVO_OFFSET;
  /* right servo  mixing */
  TIM1->CCR4 = add(reverse(yawRateCmd), pitchAngleCmd) - RIGHT_SERVO_OFFSET;

  /* double promotion here! has to be fixed! */
  if (readChannel(THROTTLE_CHANNEL) > 1100) {
    sprintf(sprintf_buf, "%.3f %.3f %.3f %.3f %.3f %.3f %d\n", (double)roll, (double)setRoll, (double)pitch, (double)setPitch, (double)Gy, (double)setYaw, readChannel(THROTTLE_CHANNEL));
    Uart_send(sprintf_buf);
  }

  /* set state */
  if (getFlyingMode() == failsafe) {
    return rclossState;
  } else
  if (getFlyingMode() == disarmed)
    return disarmedState;
  else if (getFlyingMode() == vertical && getStabilisation() == accro)
    return accroVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == accro)
    return accroHorState;
  else if (getFlyingMode() == horizontal && getStabilisation() == angle)
    return angleHorState;

  return angleVertState;
}

void *angleHorState() {
  /* roll position command */
  float setRoll = mapFloat(readChannel(ROLL_CHANNEL), 1000, 2000, -20, 20);
  int16_t rollAngleCmd = 1500 - (int16_t)ComputeCorrection(rollAxis, setRoll, roll, DELTA_T);
  rollAngleCmd = constrain(rollAngleCmd, 1000, 2000);

  /* pitch position command */
  float setPitch = mapFloat(reverse(readChannel(PITCH_CHANNEL)), 1000, 2000, -20, 20);  // this is inverted
  // add offset to pitch when SWD > 1500 for high AOA manouvers
  if (readChannel(SWD_CHANNEL) > 1500) {
    setPitch += TRANSITION_ANGLE;
  }
  int16_t pitchCorr = 1500 - (int16_t)ComputeCorrection(pitchAxis, setPitch, pitch, DELTA_T);
  int16_t pitchAngleCmd = constrain(pitchCorr, 1000, 2000);

  /* yaw postion command just controller input, no correction */
  int16_t yawDiff = map(readChannel(YAW_CHANNEL), 1000, 2000, -80, 80);
  if (yawDiff > 20 || yawDiff < -20)
    yawDiff = yawDiff;
  else
    yawDiff = 0;

  /* yaw rate correction */
  // float setYaw = map(readChannel(YAW_CHANNEL), 1000, 2000, -135, 135);
  // int16_t yawDiff = -(int16_t)ComputeCorrection(yawSpeedHor, -setYaw, Gz, DELTA_T);

  /* left motor */
  TIM2->CCR2 = constrain(readChannel(THROTTLE_CHANNEL) + yawDiff, 1000, 2000) - THROT_OFFSET;
  /* right motor */
  TIM2->CCR1 = constrain(readChannel(THROTTLE_CHANNEL) - yawDiff, 1000, 2000) - THROT_OFFSET;

  /* left servo */
  TIM1->CCR1 = add(reverse(rollAngleCmd), reverse(pitchAngleCmd)) - LEFT_SERVO_OFFSET;
  /* right servo */
  TIM1->CCR4 = add(reverse(rollAngleCmd), pitchAngleCmd) - RIGHT_SERVO_OFFSET;

  /* double promotion here! has to be fixed! */
  if (readChannel(THROTTLE_CHANNEL) > 1100) {
    sprintf(sprintf_buf, "%.3f %.3f %.3f %.3f %.3f %.3f %d\n", (double)roll, (double)setRoll, (double)pitch, (double)setPitch, (double)Gy, (double)setYaw, readChannel(THROTTLE_CHANNEL));
    Uart_send(sprintf_buf);
  }

  /* set state */
  if (getFlyingMode() == disarmed)
    return disarmedState;
  else if (getFlyingMode() == vertical && getStabilisation() == accro)
    return accroVertState;
  else if (getFlyingMode() == horizontal && getStabilisation() == accro)
    return accroHorState;
  else if (getFlyingMode() == vertical && getStabilisation() == angle)
    return angleVertState;

  return angleHorState;
}

void *rclossState() {
  /* roll position command, swapped with yaw gains! */
  float setRoll = 0;
  int16_t rollAngleCmd = (int16_t)ComputeCorrection(rollAxis, setRoll, roll, loopTime);

  /* pitch position command */
  float setPitch = PITCH_DESCENT_ANGLE;  // this is inverted
  int16_t pitchCorr = 1500 - (int16_t)ComputeCorrection(pitchAxis, setPitch, pitch, loopTime);
  int16_t pitchAngleCmd = constrain(pitchCorr, 1000, 2000);

  /* yaw rate command */
  float setYaw = 0;
  int16_t yawRateCorr = (int16_t)ComputeCorrection(yawSpeedVert, setYaw, Gy, loopTime);
  int16_t yawRateCmd = 1500 - yawRateCorr;

  /* left motor mixing*/
  TIM2->CCR2 = constrain(DESCENT_THROTTLE + rollAngleCmd, 1000, 2000) - THROT_OFFSET;
  /* right motor mixing */
  TIM2->CCR1 = constrain(DESCENT_THROTTLE - rollAngleCmd, 1000, 2000) - THROT_OFFSET;

  /* left servo mixing */
  TIM1->CCR1 = add(reverse(yawRateCmd), reverse(pitchAngleCmd)) - LEFT_SERVO_OFFSET;
  /* right servo  mixing */
  TIM1->CCR4 = add(reverse(yawRateCmd), pitchAngleCmd) - RIGHT_SERVO_OFFSET;

  /* double promotion here! has to be fixed! */
  sprintf(sprintf_buf, "%.3f %.3f %.3f %.3f %.3f %.3f %d\n", (double)roll, (double)setRoll, (double)pitch, (double)setPitch, (double)Gy, (double)setYaw, readChannel(THROTTLE_CHANNEL));
  Uart_send(sprintf_buf);

  /* set state, if signal is back return to vertical stabilized flight
     else descent till land */
  if (getFlyingMode() == vertical) {
    return angleVertState;
  } else if (pitch < PITCH_LANDED_TRASHOLD) {
    return disarmedState;
  } else {
    return rclossState;
  }

}