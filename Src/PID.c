#include "PID.h"

/* horizontal angle gains; need to be tuned! */
float rollAngleHorGains[4] = {0.005, 1550, 195, 0.1};
float pitchAngleHorGains[4] = {0.005, 2000, 180, 0.03};
float yawAngleHorGains[4] = {0.005, 800, 85, 0.1};

/* horizontal rate gains */
float yawSpeedHorGains[4] = {0.005, 100, 10, 0};

/* vertical angle gains */
float rollAngleVertGains[4] = {0.01, 500, 60, 2};
float pitchAngleVertGains[4] = {0.01, 320, 62, 0.5};
// float yawAngleVertGains[4] = {0.005, 800, 85, 0.1};

/* vertical rate gains */
float yawSpeedVertGains[4] = {0.01, 60, 0.1, 1};
float pitchSpeedVertGains[4] = {0.01, 60, 0.1, 1};
float rollSpeedVertGains[4] = {0.01, 60, 0.1, 1};

float dTermPrev[AXIS_NB];

void SetGains(int axis, float params[4]) {
  G[axis] = params[0];
  Kp[axis] = params[1];
  Kd[axis] = params[2];
  Ki[axis] = params[3];
}

float ComputeCorrection(int axis, float cmd, float pos, float dt) {
  float correction = 0;
  error[axis] = cmd - pos;

  /* anti wind up */
  // if (error[axis] < -0.5f || error[axis] > 0.5f) {
  //   integrator[axis] = integrator[axis] + error[axis];
  // } else {
  //   integrator[axis] = 0;
  // }

  /* another anti windup (on error sign change) */
  if (errorPrev[axis] * error[axis] <= 0) {
    integrator[axis] = 0;
  } else {
    integrator[axis] = integrator[axis] + error[axis];
  }

  /* D term low pass filter */
  float dTerm = Kd[axis] * ((error[axis] - errorPrev[axis]) / (dt));
  float dFiltered = (1 - D_TERM_GAIN) * dTerm +  D_TERM_GAIN * dTermPrev[axis];
  dTermPrev[axis] = dTerm;

  /* correction in us */
  correction = G[axis] * (Kp[axis] * error[axis] + dFiltered +
                          Ki[axis] * integrator[axis]);

  errorPrev[axis] = error[axis];

  return correction;
}
