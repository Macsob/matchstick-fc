
/**
  * File: EKF.c
  * Routine(s):
  *   initKalman
  *   predict
  *   update
  *   getEuler 
*/

#include "EKF.h"
#include "matrix.h"
#include "attitude.h"
#include <math.h>

float mag_A_inv[3][3] = {{1.95518283e-02, 5.88907447e-05, 1.89091297e-04},
                             {5.88907447e-05, 1.98932356e-02, 4.12818514e-05},
                             {1.89091297e-04, 4.12818514e-05, 2.07221979e-02}};

float mag_b[3][1] = {{10.3316632}, {3.82743126}, {-3.47550067}};

void getRotMx(float quaterion[7][1], float rotMx[3][3]) {
  rotMx[0][0] = pow(quaterion[0][0], 2) + pow(quaterion[1][0], 2) - pow(quaterion[2][0], 2) - pow(quaterion[3][0], 2);
  rotMx[0][1] = 2 * (quaterion[1][0] * quaterion[2][0] - quaterion[0][0] * quaterion[3][0]);
  rotMx[0][2] = 2 * (quaterion[1][0] * quaterion[3][0] + quaterion[0][0] * quaterion[2][0]);
  rotMx[1][0] = 2 * (quaterion[1][0] * quaterion[2][0] + quaterion[0][0] * quaterion[3][0]);
  rotMx[1][1] = pow(quaterion[0][0], 2) - pow(quaterion[1][0], 2) + pow(quaterion[2][0], 2) - pow(quaterion[3][0], 2);
  rotMx[1][2] = 2 * (quaterion[2][0] * quaterion[3][0] - quaterion[0][0] * quaterion[1][0]);
  rotMx[2][0] = 2 * (quaterion[1][0] * quaterion[3][0] - quaterion[0][0] * quaterion[2][0]);
  rotMx[2][1] = 2 * (quaterion[2][0] * quaterion[3][0] + quaterion[0][0] * quaterion[1][0]);
  rotMx[2][2] = pow(quaterion[0][0], 2) - pow(quaterion[1][0], 2) - pow(quaterion[2][0], 2) + pow(quaterion[3][0], 2);
}

float rad2deg(float rad) {
  return rad / PI * 180;
}

void getEuler() {
  float rotMx[3][3];
  getRotMx(xHat, rotMx);
  float test = -rotMx[2][0];
  float yawRad, pitchRad, rollRad;

  if (test > 0.99999f) {
    yawRad = 0;
    pitchRad = PI / 2;
    rollRad = atan2(rotMx[0][1], rotMx[2][0]);
  } else if (test < -0.99999f) {
    yawRad = 0;
    pitchRad = -PI / 2;
    rollRad = atan2(-rotMx[0][1], -rotMx[0][2]);
  } else {
    yawRad = atan2(rotMx[1][0], rotMx[0][0]);
    pitchRad = asin(-rotMx[2][0]);
    rollRad = atan2(rotMx[2][1], rotMx[2][2]);
  }

  // yawRad   = atan2(2.0 * (xHat[1][0] * xHat[2][0] + xHat[0][0] * xHat[3][0]), xHat[0][0] * xHat[0][0] + xHat[1][0] * xHat[1][0] - xHat[2][0] * xHat[2][0] - xHat[3][0] * xHat[3][0]);
  // pitchRad = asin(2.0 * (xHat[0][0] * xHat[2][0] - xHat[3][0] * xHat[1][0]));
  // rollRad  = atan2(2.0 * (xHat[0][0] * xHat[1][0] + xHat[2][0] * xHat[3][0]), xHat[0][0] * xHat[0][0] - xHat[1][0] * xHat[1][0] - xHat[2][0] * xHat[2][0] + xHat[3][0] * xHat[3][0]);

  /* swap axis here */
  yaw = rad2deg(yawRad);
  pitch = rad2deg(rollRad);
  roll = rad2deg(pitchRad);
}

void initKalman() {
  ones(7, 1, xHat, 0);
  xHat[0][0] = 1;
  identity(7, p, 0.1f); // 0.01 smaller p takes longer to converge
  identity(7, Q, 0.001f); // 0.0001 process noise covariance
  identity(6, R, 100.0f); // .1 measurement noise covariance
  ones(3, 1, accRef, 0);
  accRef[2][0] = 1;
  ones(3, 1, magRef, 0);
  magRef[1][0] = -1;
}

void normaliseQuat(int r, float mx[r][1]) {
  float mag = invSqrt(mx[0][0]*mx[0][0] + mx[1][0]*mx[1][0] + mx[2][0]*mx[2][0] + mx[3][0]*mx[3][0]);

  for (int i = 0; i < 4; i++) {
    mx[i][0] *= mag;
  }
}

void getAccVec(float a1, float a2, float a3, float a[3][1]) {
  a[0][0] = a1;
  a[1][0] = a2;
  a[2][0] = a3;

  float accMag = invSqrt(a1*a1 + a2*a2 + a3*a3);

  scalarMultiply(3, 1, a, accMag);
}

void getMagVec(float m1, float m2, float m3, float m[3][1]) {
  m[0][0] = m1;
  m[1][0] = m2;
  m[2][0] = m3;

  mxSub(3, 1, m, mag_b);

  float magGaussRaw[3][1];

  mxMul(3, 3, mag_A_inv, 3, 1, m, magGaussRaw);  // calculate magGaussRaw

  float rotMx[3][3];
  getRotMx(xHat, rotMx);

  float magGauss_N[3][1];
  mxMul(3, 3, rotMx, 3, 1, magGaussRaw, magGauss_N);  // calculate magGauss_N
  magGauss_N[2][0] = 0;
  scalarMultiply(3, 1, magGauss_N, invSqrt(magGauss_N[0][0]*magGauss_N[0][0] + magGauss_N[1][0]*magGauss_N[1][0]));

  float rotMxT[3][3];
  transpose(3, 3, rotMx, rotMxT);
  float magGauss_B[3][1];
  mxMul(3, 3, rotMxT, 3, 1, magGauss_N, magGauss_B);  // calculate magGauss_B

  writeOver(3, 1, m, magGauss_B);  // save over m
}

void getJacobian(float ref[3][1], float j[3][4]) {
  j[0][0] = xHatPrev[0][0] * ref[0][0] + xHatPrev[3][0] * ref[1][0] - xHatPrev[2][0] * ref[2][0];
  j[0][1] = xHatPrev[1][0] * ref[0][0] + xHatPrev[2][0] * ref[1][0] + xHatPrev[3][0] * ref[2][0];
  j[0][2] = -xHatPrev[2][0] * ref[0][0] + xHatPrev[1][0] * ref[1][0] - xHatPrev[0][0] * ref[2][0];
  j[0][3] = -xHatPrev[3][0] * ref[0][0] + xHatPrev[0][0] * ref[1][0] + xHatPrev[1][0] * ref[2][0];
  j[1][0] = -xHatPrev[3][0] * ref[0][0] + xHatPrev[0][0] * ref[1][0] + xHatPrev[1][0] * ref[2][0];
  j[1][1] = xHatPrev[2][0] * ref[0][0] - xHatPrev[1][0] * ref[1][0] + xHatPrev[0][0] * ref[2][0];
  j[1][2] = xHatPrev[1][0] * ref[0][0] + xHatPrev[2][0] * ref[1][0] + xHatPrev[3][0] * ref[2][0];
  j[1][3] = -xHatPrev[0][0] * ref[0][0] - xHatPrev[3][0] * ref[1][0] + xHatPrev[2][0] * ref[2][0];
  j[2][0] = xHatPrev[2][0] * ref[0][0] - xHatPrev[1][0] * ref[1][0] + xHatPrev[0][0] * ref[2][0];
  j[2][1] = xHatPrev[3][0] * ref[0][0] - xHatPrev[0][0] * ref[1][0] - xHatPrev[1][0] * ref[2][0];
  j[2][2] = xHatPrev[0][0] * ref[0][0] + xHatPrev[3][0] * ref[1][0] - xHatPrev[2][0] * ref[2][0];
  j[2][3] = xHatPrev[1][0] * ref[0][0] + xHatPrev[2][0] * ref[1][0] + xHatPrev[3][0] * ref[2][0];

  scalarMultiply(3, 4, j, 2);
}

void predictAccMag(float AccMag[6][1]) {
  /* Acc */
  float hPrime_a[3][4];
  getJacobian(accRef, hPrime_a);

  float rotMx[3][3];
  getRotMx(xHatBar, rotMx);
  float rotMxT[3][3];
  transpose(3, 3, rotMx, rotMxT);

  float accBar[3][1];
  mxMul(3, 3, rotMxT, 3, 1, accRef, accBar);

  /* Mag */
  float hPrime_m[3][4];
  getJacobian(magRef, hPrime_m);

  float magBar[3][1];
  mxMul(3, 3, rotMxT, 3, 1, magRef, magBar);

  /* C */
  float tmp1[3][7];
  float tmp2[3][7];
  float Z_3x3[3][3];
  ones(3, 3, Z_3x3, 0);
  concat_1(3, 4, 3, hPrime_a, Z_3x3, tmp1);
  concat_1(3, 4, 3, hPrime_m, Z_3x3, tmp2);
  concat_0(3, 3, 7, tmp1, tmp2, C);

  /* AccMag */
  concat_0(3, 3, 1, accBar, magBar, AccMag);
}

void predict(float w1, float w2, float w3, float dt) {
  float w[3][1];

  /* important! has to be rad/s */
  w[0][0] = w1 * DEG_TO_RAD;
  w[1][0] = w2 * DEG_TO_RAD;
  w[2][0] = w3 * DEG_TO_RAD;

  float Sq[4][3];  // r = 4, c = 3

  Sq[0][0] = -xHat[1][0];
  Sq[0][1] = -xHat[2][0];
  Sq[0][2] = -xHat[3][0];
  Sq[1][0] = xHat[0][0];
  Sq[1][1] = -xHat[3][0];
  Sq[1][2] = xHat[2][0];
  Sq[2][0] = xHat[3][0];
  Sq[2][1] = xHat[0][0];
  Sq[2][2] = -xHat[1][0];
  Sq[3][0] = -xHat[2][0];
  Sq[3][1] = xHat[1][0];
  Sq[3][2] = xHat[0][0];

  float I_3x3[3][3];
  identity(3, I_3x3, 1);  // set identity

  float I_4x4[4][4];
  identity(4, I_4x4, 1);  // set identity

  float Z_3x4[3][4];
  ones(3, 4, Z_3x4, 0);  // set all zeros

  float Z_3x3[3][3];
  ones(3, 3, Z_3x3, 0);  // set all zeros

  scalarMultiply(4, 3, Sq, -dt / 2);

  float tmp1[4][7];
  concat_1(4, 4, 3, I_4x4, Sq, tmp1);

  float tmp2[3][7];
  concat_1(3, 4, 3, Z_3x4, I_3x3, tmp2);

  concat_0(4, 3, 7, tmp1, tmp2, A);  // set A[7][7]

  scalarMultiply(4, 3, Sq, -1);
  concat_0(4, 3, 3, Sq, Z_3x3, B);  // set B[7][3]

  float AxHat[7][1];
  mxMul(7, 7, A, 7, 1, xHat, AxHat);

  float Bw[7][1];
  mxMul(7, 3, B, 3, 1, w, Bw);

  mxAdd(7, 1, AxHat, Bw, xHatBar); // xHatBar = AxHar + Bw

  normaliseQuat(7, xHatBar);
  writeOver(7, 1, xHatPrev, xHat);  // xHatPrev = xHat

  predictAccMag(yHatBar);

  float Ap[7][7];
  mxMul(7, 7, A, 7, 7, p, Ap);

  float AT[7][7];
  transpose(7, 7, A, AT);

  float ApAT[7][7];
  mxMul(7, 7, Ap, 7, 7, AT, ApAT);

  mxAdd(7, 7, ApAT, Q, pBar);

}

void update(float a1, float a2, float a3, float m1, float m2, float m3) {
  float CT[7][6];
  transpose(6, 7, C, CT);

  float CpBar[6][7];
  mxMul(6, 7, C, 7, 7, pBar, CpBar);

  float CpBarCT[6][6];
  mxMul(6, 7, CpBar, 7, 6, CT, CpBarCT);
  mxAddEq(6, 6, CpBarCT, R);

  float inv[6][6];
  // mxInverse(6, CpBarCT, inv);
  invert(6, CpBarCT, inv);

  float pBarCT[7][6];
  mxMul(7, 7, pBar, 7, 6, CT, pBarCT);

  mxMul(7, 6, pBarCT, 6, 6, inv, K);  // set K[7][6]

  float magGauss_B[3][1];
  getMagVec(m1, m2, m3, magGauss_B);

  float accVec_B[3][1];
  getAccVec(a1, a2, a3, accVec_B);

  float measurement[6][1];
  concat_0(3, 3, 1, accVec_B, magGauss_B, measurement);
  mxSub(6, 1, measurement, yHatBar);

  float Kmeasurement[7][1];
  mxMul(7, 6, K, 6, 1, measurement, Kmeasurement);

  mxAdd(7, 1, xHatBar, Kmeasurement, xHat);  // xHat = xHatBar + matmul(k, measurment - yHatBar)
  normaliseQuat(7, xHat);

  float I_7x7[7][7];
  identity(7, I_7x7, 1);

  float KC[7][7];
  mxMul(7, 6, K, 6, 7, C, KC);

  mxSub(7, 7, I_7x7, KC);  // I - KC

  mxMul(7, 7, I_7x7, 7, 7, pBar, p);
}
