/*
 * Attitude estimation using Extended Kalman Filter on MPU6050 and HMC5883L data
 */
#include <math.h>
#include "attitude.h"
#include "matrix.h"
#include "ringbuffer.h"
#include <stdio.h>

I2C_HandleTypeDef hi2c1;

int16_t gyroOffsets[AXIS_NUM] = {0, 0, 0};

/** Write multiple bits in an 8-bit device register.
 * @param dev_addr 	I2C slave device address
 * @param reg_addr 	Register regAddr to write to
 * @param start_bit First bit position to write (0-7)
 * @param len 		Number of bits to write (not more than 8)
 * @param data 		Right-aligned value to write
 * @return Status of operation (0 = success, <0 = error)
 */
HAL_StatusTypeDef writeBits(uint8_t dev_addr, uint8_t reg_addr, uint8_t start_bit, uint8_t len, uint8_t data) {
/*     010 value to write
  76543210 bit numbers
     xxx   args: bitStart=4, length=3
  00011100 mask byte
  10101111 original value (sample)
  10100011 original & ~mask
  10101011 masked | value */

  uint8_t b;
  HAL_StatusTypeDef err;

  if ((err = HAL_I2C_Mem_Read(&hi2c1, dev_addr, reg_addr, 1, &b, 1, 1000)) == 0) {
    uint8_t mask = ((1 << len) - 1) << (start_bit - len + 1);
    data <<= (start_bit - len + 1);  // shift data into correct position
    data &= mask;                    // zero all non-important bits in data
    b &= ~(mask);                    // zero all important bits in existing byte
    b |= data;                       // combine data with existing byte

    return HAL_I2C_Mem_Write(&hi2c1, dev_addr, reg_addr, 1, &b, 1, 1000);
  } else {
    return err;
  }
}

/** Read a single bit from a 8-bit device register.
 * @param dev_addr 	I2C slave device address
 * @param reg_addr 	Register regAddr to read from
 * @param bitn 		Bit position to read (0-15)
 * @param data 		Container for single bit value
 * @return HAL status of read operation
 */
HAL_StatusTypeDef readBit(I2C_HandleTypeDef *hi2c, uint8_t dev_addr, uint8_t reg_addr, uint8_t bitn, uint8_t *data, uint32_t timeout) {
	int8_t err;

	err = HAL_I2C_Mem_Read(hi2c, dev_addr, reg_addr, I2C_MEMADD_SIZE_8BIT, data, 1, timeout);
	*data = (*data >> bitn) & 0x01;

	return err;
}

/** Read slave address
 * @param num Slave number (0-3)
 * @return Current address for specified slave
 * @see MPU6050_RA_I2C_SLV0_ADDR
 */
uint8_t MPU6050_getSlaveAddress(uint8_t num, uint32_t timeout) {
    if (num > 3) return 0;
    HAL_I2C_Mem_Read(&hi2c1 ,MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR + num*3, I2C_MEMADD_SIZE_8BIT, AHRS_buffer_DMA, 1, timeout);
    return AHRS_buffer_DMA[0];
}

/** Set the I2C address of the specified slave (0-3).
 * @param num Slave number (0-3)
 * @param address New address for specified slave
 * @see getSlaveAddress()
 * @see MPU6050_RA_I2C_SLV0_ADDR
 */
void MPU6050_setSlaveAddress(uint8_t num, uint8_t address, uint32_t timeout) {
    if (num > 3) return;
    HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR + num*3, I2C_MEMADD_SIZE_8BIT, &address, 1, timeout);
}

/** Get the enabled value for the specified slave (0-3).
 * When set to 1, this bit enables Slave 0 for data transfer operations. When
 * cleared to 0, this bit disables Slave 0 from data transfer operations.
 * @param num Slave number (0-3)
 * @return Current enabled value for specified slave
 * @see MPU6050_RA_I2C_SLV0_CTRL
 */
uint8_t MPU6050_getSlaveEnabled(uint8_t num, uint32_t timeout) {
    if (num > 3) return 0;
    readBit(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL + num*3, MPU6050_I2C_SLV_EN_BIT, AHRS_buffer_DMA, timeout);
    return AHRS_buffer_DMA[0];
}

/** Set the enabled value for the specified slave (0-3).
 * @param num Slave number (0-3)
 * @param enabled New enabled value for specified slave 1 or 0
 * @see getSlaveEnabled()
 * @see MPU6050_RA_I2C_SLV0_CTRL
 */
void MPU6050_setSlaveEnabled(uint8_t num, uint8_t enabled) {
    if (num > 3) return;
    writeBits(MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL + num*3, MPU6050_I2C_SLV_EN_BIT, 1, enabled);
}

/** Get the active internal register for the specified slave (0-3).
 * Read/write operations for this slave will be done to whatever internal
 * register address is stored in this MPU register.
 *
 * The MPU-6050 supports a total of five slaves, but Slave 4 has unique
 * characteristics, and so it has its own functions.
 *
 * @param num Slave number (0-3)
 * @return Current active register for specified slave
 * @see MPU6050_RA_I2C_SLV0_REG
 */
uint8_t MPU6050_getSlaveRegister(uint8_t num, uint32_t timeout) {
    if (num > 3) return 0;
    HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG + num*3, I2C_MEMADD_SIZE_8BIT, AHRS_buffer_DMA, 1, timeout);
    return AHRS_buffer_DMA[0];
}
/** Set the active internal register for the specified slave (0-3).
 * @param num Slave number (0-3)
 * @param reg New active register for specified slave
 * @see getSlaveRegister()
 * @see MPU6050_RA_I2C_SLV0_REG
 */
void MPU6050_setSlaveRegister(uint8_t num, uint8_t reg, uint32_t timeout) {
    if (num > 3) return;
    HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG + num*3, I2C_MEMADD_SIZE_8BIT, &reg, 1, timeout);
}

void readSlaveRegisters(uint8_t AuxDevAddr, uint8_t AuxMemAddr, uint8_t* buf, uint32_t count, uint32_t timeout) {
  uint8_t ctrl = I2C_READ_FLAG | count;
  uint8_t devAddr = AuxDevAddr | I2C_READ_FLAG;

  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR, I2C_MEMADD_SIZE_8BIT, &devAddr, 1, timeout);
  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG, I2C_MEMADD_SIZE_8BIT, &AuxMemAddr, 1, timeout);
  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL, I2C_MEMADD_SIZE_8BIT, &ctrl, 1, timeout);

  HAL_Delay(2); // wait for registers fill

  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, MPU6050_RA_EXT_SENS_DATA_00, I2C_MEMADD_SIZE_8BIT, buf, count, timeout);
}

void writeSlaveRegister(uint8_t AuxDevAddr, uint8_t AuxMemAddr, uint8_t data, uint32_t timeout) {
  uint8_t ctrl = 0x81;

  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR, I2C_MEMADD_SIZE_8BIT, &AuxDevAddr, 1, timeout);
  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG, I2C_MEMADD_SIZE_8BIT, &AuxMemAddr, 1, timeout);
  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_DO, I2C_MEMADD_SIZE_8BIT, &data, 1, timeout);
  HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL, I2C_MEMADD_SIZE_8BIT, &ctrl, 1, timeout);
  // writeBits(MPU6050_ADDR, )
}

void MPU6050_Init() {
  uint8_t check;
  uint8_t Data;

  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, WHO_AM_I_REG, 1, &check, 1, 1000);  // check device ID

  if (check == 104) {  // if the device is present

    // to wake up the sensor write 0's to power management register 0x6B
    Data = 0;
    HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, POWER_MANAGEMENT_REG, 1, &Data, 1, 1000);

    // set clock source
    writeBits(MPU6050_ADDR, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, MPU6050_CLOCK_PLL_XGYRO);

    // to set data rate to 1KHz
    Data = 0x07;
    HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, SMPLRT_DIV_REG, 1, &Data, 1, 1000);

    // set acc configuration
    Data = 0x02;
    writeBits(MPU6050_ADDR, ACCEL_CONFIG_REG, MPU6050_ACONFIG_AFS_SEL_BIT, MPU6050_ACONFIG_AFS_SEL_LENGTH, Data);

    // set gyro configuration
    Data = 0x02;
    writeBits(MPU6050_ADDR, GYRO_CONFIG_REG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, Data);

    // bypass enable so that HMC5883 can communicate, auxiliary I2C pass-through
    // Data = 0x02;
    // HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, MPU6050_RA_INT_PIN_CFG, I2C_MEMADD_SIZE_8BIT, &Data, I2C_MEMADD_SIZE_8BIT, 1000);
    
    // enable low pass filter with cut off at 42Hz
    writeBits(MPU6050_ADDR, MPU6050_RA_CONFIG, MPU6050_CFG_DLPF_CFG_BIT, MPU6050_CFG_DLPF_CFG_LENGTH, MPU6050_DLPF_BW_42);

    // enable I2C master mode (write 1 in MST_EN_BIT)
    writeBits(MPU6050_ADDR, MPU6050_RA_USER_CTRL, MPU6050_USERCTRL_I2C_MST_EN_BIT, 1, 1);

    // set master I2C speed
    writeBits(MPU6050_ADDR, MPU6050_RA_I2C_MST_CTRL, MPU6050_I2C_MST_CLK_BIT, MPU6050_I2C_MST_CLK_LENGTH, 0x0d);

    // set up HMC
    readSlaveRegisters(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_ID_A, &check, 1, 1000);

    // check if HMC is there
    if (check == 0x48) {
      sprintf(sprintf_buf, "check: %x\n", check);
      Uart_send(sprintf_buf);
      // write CONFIG_A register
      Data = (HMC5883L_AVERAGING_8 << (HMC5883L_CRA_AVERAGE_BIT - HMC5883L_CRA_AVERAGE_LENGTH + 1)) |
             (HMC5883L_RATE_15 << (HMC5883L_CRA_RATE_BIT - HMC5883L_CRA_RATE_LENGTH + 1)) |
             (HMC5883L_BIAS_NORMAL << (HMC5883L_CRA_BIAS_BIT - HMC5883L_CRA_BIAS_LENGTH + 1));
      writeSlaveRegister(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_CONFIG_A, Data, 1000);

      // write CONFIG_B register
      Data = HMC5883L_GAIN_1090 << (HMC5883L_CRB_GAIN_BIT - HMC5883L_CRB_GAIN_LENGTH + 1);
      writeSlaveRegister(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_CONFIG_B, Data, 1000);

      // write MODE register
      Data = HMC5883L_MODE_CONTINUOUS << (HMC5883L_MODEREG_BIT - HMC5883L_MODEREG_LENGTH + 1);
      writeSlaveRegister(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_MODE, Data, 1000);

      // last read will be repeated at data transfer rate
      readSlaveRegisters(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_DATAX_H, &AHRS_buffer_DMA[14], 6, 1000);

    } else {
      HAL_Delay(20);
      MPU6050_Init();
    }

    // uint8_t test = MPU6050_getSlaveRegister(0);
    // readBit(&hi2c1, MPU6050_ADDR, MPU6050_RA_USER_CTRL, MPU6050_USERCTRL_I2C_MST_EN_BIT, &test);

    // for (int i = 0; i < 100; i++) {
    //   readSlaveRegisters(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_DATAX_H, &AHRS_buffer_DMA[14], 6, 1000);
    //   HAL_Delay(100);

    //   sprintf(sprintf_buf, "mag x_H: %d\n", AHRS_buffer_DMA[15]);
    //   Uart_send(sprintf_buf);
    // }

  } else {
    HAL_Delay(20);
    MPU6050_Init();
  }
}

void MPU6050_Read_Accel(void) {
  uint8_t Rec_data[6];

  // read 6 bytes starting from ACCEL_XOUT_H register
  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, ACCEL_XOUT_H_REG, 1, Rec_data, 6, 1000);

  Accel_X_Raw = (int16_t)(Rec_data[0] << 8 | Rec_data[1]);
  Accel_Y_Raw = (int16_t)(Rec_data[2] << 8 | Rec_data[3]);
  Accel_Z_Raw = (int16_t)(Rec_data[4] << 8 | Rec_data[5]);

  // convert raw values to g
  Ax = Accel_X_Raw / 4096.0f;
  Ay = Accel_Y_Raw / 4096.0f;
  Az = Accel_Z_Raw / 4096.0f;
}

void MPU6050_Read_Gyro(void) {
  uint8_t Rec_data[6];

  // read 6 bytes starting from GYRO_XOUT_H register
  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, GYRO_XOUT_H_REG, 1, Rec_data, 6, 1000);

  Gyro_X_Raw = (int16_t)(Rec_data[0] << 8 | Rec_data[1]);
  Gyro_Y_Raw = (int16_t)(Rec_data[2] << 8 | Rec_data[3]);
  Gyro_Z_Raw = (int16_t)(Rec_data[4] << 8 | Rec_data[5]);

  // convert raw values to deg/s
  // subtract offsets computed during initialisation

  Gx = (Gyro_X_Raw - gyroOffsets[0]) / 32.8f;
  Gy = (Gyro_Y_Raw - gyroOffsets[1]) / 32.8f;
  Gz = (Gyro_Z_Raw - gyroOffsets[2]) / 32.8f;
}

void computeMean(int16_t sampleList[], int size, float *mean) {
  (*mean) = 0;
  for (int sample = 0; sample < size; sample++) {
    (*mean) = (*mean) + sampleList[sample];
  }
  (*mean) = (*mean) / size;
}

int computeOffsets() {
  int16_t gyroRAW[AXIS_NUM][SAMPLE_NUM];
  float mean = 0;

  for (int axis = 0; axis < AXIS_NUM; axis++)
    for (int sample = 0; sample < SAMPLE_NUM; sample++) gyroRAW[axis][sample] = 0;
  
  // get samples
  for (int sample = 0; sample < SAMPLE_NUM; sample++) {
    MPU6050_Read_Gyro();
    gyroRAW[0][sample] = Gyro_X_Raw;
    gyroRAW[1][sample] = Gyro_Y_Raw;
    gyroRAW[2][sample] = Gyro_Z_Raw;
    HAL_Delay(100);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
  }

  // compute mean
  for (int axis = 0; axis < AXIS_NUM; axis++) {
    computeMean(gyroRAW[axis], SAMPLE_NUM, &mean);
    gyroOffsets[axis] = (int16_t)(mean);
  }

  if (mean != 0) return 1;
  else
  {
    return -1;
  }
}

void HMC5883L_Init(void) {
  uint8_t check;
  uint8_t Data;

  // if (HAL_I2C_IsDeviceReady(&hi2c1, HMC5883L_ADDRESS, 10, 100) == HAL_OK) {

  // } else {
  //   HMC5883L_Init();
  // }

  // check if Identification Register A is == 0x48
  // could also check for B and C
  HAL_I2C_Mem_Read(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_ID_A, I2C_MEMADD_SIZE_8BIT, &check, 1, 1000);

  if (check == 0x48) {
    // write CONFIG_A register
    Data = (HMC5883L_AVERAGING_8 << (HMC5883L_CRA_AVERAGE_BIT - HMC5883L_CRA_AVERAGE_LENGTH + 1)) |
           (HMC5883L_RATE_15 << (HMC5883L_CRA_RATE_BIT - HMC5883L_CRA_RATE_LENGTH + 1)) |
           (HMC5883L_BIAS_NORMAL << (HMC5883L_CRA_BIAS_BIT - HMC5883L_CRA_BIAS_LENGTH + 1));

    HAL_I2C_Mem_Write(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_CONFIG_A, 1, &Data, 1, 1000);

    // write CONFIG_B register
    Data = HMC5883L_GAIN_1090 << (HMC5883L_CRB_GAIN_BIT - HMC5883L_CRB_GAIN_LENGTH + 1);

    HAL_I2C_Mem_Write(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_CONFIG_B, 1, &Data, 1, 1000);

    // write MODE register
    Data = HMC5883L_MODE_CONTINUOUS << (HMC5883L_MODEREG_BIT - HMC5883L_MODEREG_LENGTH + 1);

    HAL_I2C_Mem_Write(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_MODE, 1, &Data, 1, 1000);
  } else {
    HMC5883L_Init();
  }
}

void HMC5883L_Read_Mag(void) {
  uint8_t Rec_data[6];

  // read 6 bytes starting from HMC5883L_RA_DATAX_H register
  HAL_I2C_Mem_Read(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_DATAX_H, 1, Rec_data, 6, 1000);

  // awkward order XZY
  Mag_X_Raw = (int16_t)(Rec_data[0] << 8 | Rec_data[1]);
  Mag_Z_Raw = (int16_t)(Rec_data[2] << 8 | Rec_data[3]);
  Mag_Y_Raw = (int16_t)(Rec_data[4] << 8 | Rec_data[5]);

  Mx = Mag_X_Raw * 0.092f;
  My = Mag_Y_Raw * 0.092f;
  Mz = Mag_Z_Raw * 0.092f;
}

float get_accel_z(void) {
  return Az;
}

void getMotion9(int16_t* ax, int16_t* ay, int16_t* az, int16_t* gx, int16_t* gy, int16_t* gz, int16_t* mx, int16_t* my, int16_t* mz) {
  uint8_t MPU_buffer[14];
  uint8_t HMC_buffer[6];
  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, ACCEL_XOUT_H_REG, 1, MPU_buffer, 14, 10);
  HAL_I2C_Mem_Read(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_DATAX_H, 1, HMC_buffer, 6, 10);
  *ax = (((int16_t)MPU_buffer[0]) << 8) | MPU_buffer[1];
  *ay = (((int16_t)MPU_buffer[2]) << 8) | MPU_buffer[3];
  *az = (((int16_t)MPU_buffer[4]) << 8) | MPU_buffer[5];
  *gx = (((int16_t)MPU_buffer[8]) << 8) | MPU_buffer[9];
  *gy = (((int16_t)MPU_buffer[10]) << 8) | MPU_buffer[11];
  *gz = (((int16_t)MPU_buffer[12]) << 8) | MPU_buffer[13];
  *mx = (int16_t)(HMC_buffer[0] << 8 | HMC_buffer[1]);
  *mz = (int16_t)(HMC_buffer[2] << 8 | HMC_buffer[3]);
  *my = (int16_t)(HMC_buffer[4] << 8 | HMC_buffer[5]);
}

void convertRaw() {
  Ax = Accel_X_Raw / 4096.0f;
  Ay = Accel_Y_Raw / 4096.0f;
  Az = Accel_Z_Raw / 4096.0f;
  Gx = (Gyro_X_Raw - gyroOffsets[0]) / 32.8f;
  Gy = (Gyro_Y_Raw - gyroOffsets[1]) / 32.8f;
  Gz = (Gyro_Z_Raw - gyroOffsets[2]) / 32.8f;
  Mx = Mag_X_Raw * 0.092f;
  My = Mag_Y_Raw * 0.092f;
  Mz = Mag_Z_Raw * 0.092f;
}

void getMotion9_DMA() {
  Accel_X_Raw = (((int16_t)AHRS_buffer_DMA[0]) << 8) | AHRS_buffer_DMA[1];
  Accel_Y_Raw = (((int16_t)AHRS_buffer_DMA[2]) << 8) | AHRS_buffer_DMA[3];
  Accel_Z_Raw = (((int16_t)AHRS_buffer_DMA[4]) << 8) | AHRS_buffer_DMA[5];
  Gyro_X_Raw = (((int16_t)AHRS_buffer_DMA[8]) << 8) | AHRS_buffer_DMA[9];
  Gyro_Y_Raw = (((int16_t)AHRS_buffer_DMA[10]) << 8) | AHRS_buffer_DMA[11];
  Gyro_Z_Raw = (((int16_t)AHRS_buffer_DMA[12]) << 8) | AHRS_buffer_DMA[13];
  // Mag_X_Raw = (int16_t)(HMC_buffer_DMA[0] << 8 | HMC_buffer_DMA[1]);
  // Mag_Z_Raw = (int16_t)(HMC_buffer_DMA[2] << 8 | HMC_buffer_DMA[3]);
  // Mag_Y_Raw = (int16_t)(HMC_buffer_DMA[4] << 8 | HMC_buffer_DMA[5]);
  Mag_X_Raw = (int16_t)(AHRS_buffer_DMA[14] << 8 | AHRS_buffer_DMA[15]);
  Mag_Z_Raw = (int16_t)(AHRS_buffer_DMA[16] << 8 | AHRS_buffer_DMA[17]);
  Mag_Y_Raw = (int16_t)(AHRS_buffer_DMA[18] << 8 | AHRS_buffer_DMA[19]);
}