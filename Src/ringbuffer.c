/*
 * ringbuffer.c
 */

#include "ringbuffer.h"
#include <string.h>
#include <math.h>
#include <stdio.h>

/* define the UART */

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

#define uart1 &huart1
#define uart2 &huart2

/* put the following in the ISR -> stm32f3xx_it.c

extern void Uart_isr (UART_HandleTypeDef *huart);

*/

char msgRadio[sizeof(int)] = "";
char msgFloat[6] = "";  // float 4 words, double 8 words, %.2f 6 words

// char byteData1[sizeof(float)];
// char byteData2[sizeof(float)];
// char byteData3[sizeof(float)];

uint8_t ibusIndex = 0;
uint16_t rcValue[IBUS_MAXCHANNELS];
uint16_t radioInput[IBUS_MAXCHANNELS];
uint8_t ibusBuffer[IBUS_FRAME];

ring_buffer rx_buffer = {{0}, 0, 0};
ring_buffer tx_buffer = {{0}, 0, 0};

ring_buffer *_rx_buffer;
ring_buffer *_tx_buffer;

void Ringbuf_init(void) {
  /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
  // __HAL_UART_ENABLE_IT(uart1, UART_IT_ERR);

  /* Enable the UART Data Register not empty Interrupt */
  __HAL_UART_ENABLE_IT(uart1, UART_IT_RXNE);
  
  /* with uart2 RXNE processor hungs up when increasing throttle */
  // __HAL_UART_ENABLE_IT(uart2, UART_IT_ERR);
  // __HAL_UART_ENABLE_IT(uart2, UART_IT_RXNE);
  // __HAL_UART_ENABLE_IT(uart2, UART_IT_TXE);
  
  _rx_buffer = &rx_buffer;
  _tx_buffer = &tx_buffer;
  
}

void store_char(unsigned char c, ring_buffer *buffer) {
  int i = (unsigned int)(buffer->head + 1) % UART_BUFFER_SIZE;

  /* If we should be storing the received character into the location
  just before the tail (meaning that the head would advance to the
  current location of the tail), we're about to overflow the buffer
  and so we don't write the character or advance the head. */
  if (i != buffer->tail) {
    buffer->buffer[buffer->head] = c;
    buffer->head = i;
  }
}

int Uart_read(void) {
  if (_rx_buffer->head == _rx_buffer->tail) {  // if the head isn't ahead of the tail, we don't have any characters
    return -1;
  } else {
    unsigned char c = _rx_buffer->buffer[_rx_buffer->tail];                      // read from buffer
    _rx_buffer->tail = (unsigned int)(_rx_buffer->tail + 1) % UART_BUFFER_SIZE;  // increment tail
    return c;
  }
}

void Uart_write(char c) {
//  if (c > 0) {
    int i = (_tx_buffer->head + 1) % UART_BUFFER_SIZE;

    /* If the output buffer is full, there's nothing for it other than to
    wait for the interrupt handler to empty it a bit
    ???: return 0 here instead? */
    // remove that and overwrite buffer then clean up garbage data
    // while (i == _tx_buffer->tail) {
    // };
    // keep more recent information

    _tx_buffer->buffer[_tx_buffer->head] = (char)c;
    _tx_buffer->head = i;

    __HAL_UART_ENABLE_IT(uart2, UART_IT_TXE);  // Enable UART transmission interrupt
//  }
}

// void Uart_print_int(int c) {
//   sprintf(msgRadio, "%d", c);
//   for (int i = 0; i < sizeof(int); i++) {
//     uint8_t a = msgRadio[i];
//     Uart_write(a);
//   }
// }

/* sprintf adds a NULL byte! implicit conversion from float to double!*/
// void Uart_print_float(float c) {
//   sprintf(msgFloat, "%.2f", c);
//   for (int i = 0; i < 6; i++) {
//     uint8_t a = msgFloat[i];
//     Uart_write(a);
//   }
//   Uart_write(0x20);
// }

// void printFloatFixedPrecision(char* str, float x, int precision) {
//   int integerPart = x;

//   int pos = sprintf(str, "%d.", integerPart);
//   for (int i = 0; i < precision; ++i) {
//     int digit = ((x - integerPart) * powf(10, i)) % 10;
//     pos += sprintf(str + pos, "%d", digit);
//   }
// }

// void floatToString(char* str, float x, int precision) {
//   int integerPart = (int)x;
//   int decimalPart = ((int)(x*precision)%precision);

//   sprintf(str, "%d.%d", integerPart, decimalPart);
// }

void Uart_send(char* s) {
  for (int i = 0; i < strlen(s); i++) {
    Uart_write(*(s+i));
  }
}

// void Uart_print_mx(int r, int c, float mx[r][c]) {
//   unsigned int i, j;
//   for (i = 0; i < r; i++) {
//     for (j = 0; j < c; j++) {
//       Uart_print_float(mx[i][j]);
//     }
//     Uart_write(NEW_LINE_CHAR);
//   }
// }


int IsDataAvailable(void) {
  return (uint16_t)(UART_BUFFER_SIZE + _rx_buffer->head - _rx_buffer->tail) % UART_BUFFER_SIZE;
}

void pseudoIBUS(void) {
  while (IsDataAvailable()) {
    uint8_t data = Uart_read();

    if (ibusIndex == 0 && data != IBUS_SYNCBYTE) {
      ibusIndex = 0;
      return;
    }

    if (ibusIndex == IBUS_FRAME) {
      ibusIndex = 0;

      rcValue[0] = (ibusBuffer[2] << 8) + ibusBuffer[1];
      rcValue[1] = (ibusBuffer[4] << 8) + ibusBuffer[3];
      rcValue[2] = (ibusBuffer[6] << 8) + ibusBuffer[5];
      rcValue[3] = (ibusBuffer[8] << 8) + ibusBuffer[7];
      rcValue[4] = (ibusBuffer[10] << 8) + ibusBuffer[9];
      rcValue[5] = (ibusBuffer[12] << 8) + ibusBuffer[11];
      rcValue[6] = (ibusBuffer[14] << 8) + ibusBuffer[13];
      rcValue[7] = (ibusBuffer[16] << 8) + ibusBuffer[15];
      rcValue[8] = (ibusBuffer[18] << 8) + ibusBuffer[17];
      rcValue[9] = (ibusBuffer[20] << 8) + ibusBuffer[19];
      rcValue[10] = (ibusBuffer[22] << 8) + ibusBuffer[21];
      rcValue[11] = (ibusBuffer[24] << 8) + ibusBuffer[23];
      rcValue[12] = (ibusBuffer[26] << 8) + ibusBuffer[25];
      rcValue[13] = (ibusBuffer[28] << 8) + ibusBuffer[27];

      int checksum = (ibusBuffer[30] << 8) + ibusBuffer[29];
      int myChecksum = 0;

      for (int c = 0; c < 14; c++) {
        myChecksum += rcValue[c];
      }

      if (myChecksum == checksum) {
        for (int c = 0; c < 14; c++) {
          radioInput[c] = rcValue[c];
        }

      } else {
        return;
      }
    } else {
      ibusBuffer[ibusIndex] = data;
      ibusIndex++;
    }
  }
}

// FS-iA6B is sending data even when controller is off!
void IBUS(void) {
  while (IsDataAvailable()) {
    uint32_t now = HAL_GetTick();
    if (now - last >= PROTOCOL_TIMEGAP)
    {
      state = GET_LENGTH;
    }
    last = now;
    
    uint8_t v = Uart_read();
    switch (state)
    {
      case GET_LENGTH:
        if (v <= PROTOCOL_LENGTH)
        {
          ptr = 0;
          len = v - PROTOCOL_OVERHEAD;
          chksum = 0xFFFF - v;
          state = GET_DATA;
        }
        else
        {
          state = DISCARD;
        }
        break;

      case GET_DATA:
        buffer[ptr++] = v;
        chksum -= v;
        if (ptr == len)
        {
          state = GET_CHKSUML;
        }
        break;
        
      case GET_CHKSUML:
        lchksum = v;
        state = GET_CHKSUMH;
        break;

      case GET_CHKSUMH:
        /* Validate checksum */
        if (chksum == (v << 8) + lchksum)
        {
          /* Execute command - we only know command 0x40 */
          switch (buffer[0])
          {
            case PROTOCOL_COMMAND40:
              /* Valid - extract channel data */
              for (uint8_t i = 1; i < PROTOCOL_CHANNELS * 2 + 1; i += 2)
              {
                channel[i / 2] = buffer[i] | (buffer[i + 1] << 8);
              }
              break;

            default:
              break;
          }
        }
        state = DISCARD;
        break;

      case DISCARD:
      default:
        break;
    }
  }
}

uint16_t readChannel(uint8_t channelNr) {
  if (channelNr < PROTOCOL_CHANNELS) {
    return channel[channelNr];
  } else {
    return 0;
  }
}

void Uart_isr_rx(UART_HandleTypeDef *huart) {
  uint32_t isrflags = READ_REG(huart->Instance->ISR);
  uint32_t cr1its = READ_REG(huart->Instance->CR1);

  /* if DR is not empty and the Rx Int is enabled */
  if (((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET)) {
    /******************

         *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
         *          error) and IDLE (Idle line detected) flags are cleared by software
         *          sequence: a read operation to USART_SR register followed by a read
         *          operation to USART_DR register.
         * @note   RXNE flag can be also cleared by a read to the USART_DR register.
         * @note   TC flag can be also cleared by software sequence: a read operation to
         *          USART_SR register followed by a write operation to USART_DR register.
         * @note   TXE flag is cleared only by a write to the USART_DR register.

    *********************/
    huart->Instance->ISR;                   // Read status register
    unsigned char c = huart->Instance->RDR; // Read data register
    store_char(c, _rx_buffer);              // store data in buffer
    return;
  }
}

void Uart_isr_tx(UART_HandleTypeDef *huart) {
  uint32_t isrflags = READ_REG(huart->Instance->ISR);
  uint32_t cr1its = READ_REG(huart->Instance->CR1);

  /*If interrupt is caused due to Transmit Data Register Empty */
  if (((isrflags & USART_ISR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET)) {
    if (tx_buffer.head == tx_buffer.tail) {
      /* Buffer empty, so disable interrupts */
      __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);

    }

    else {
      /* There is more data in the output buffer. Send the next byte */
      unsigned char c = tx_buffer.buffer[tx_buffer.tail];
      tx_buffer.tail = (tx_buffer.tail + 1) % UART_BUFFER_SIZE;

      /******************
      *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
      *          error) and IDLE (Idle line detected) flags are cleared by software
      *          sequence: a read operation to USART_SR register followed by a read
      *          operation to USART_DR register.
      * @note   RXNE flag can be also cleared by a read to the USART_DR register.
      * @note   TC flag can be also cleared by software sequence: a read operation to
      *          USART_SR register followed by a write operation to USART_DR register.
      * @note   TXE flag is cleared only by a write to the USART_DR register.

      *********************/

      huart->Instance->ISR;
      huart->Instance->TDR = c;
    }
    return;
  }
}
