#include "matrix.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define SUCC 1
#define FAIL -1

static int row_scalar_multiply(int size, float mx[size][size], int row, float factor);

/* type Name[row][column] */


void identity(int size, float mx[size][size], float val) {
  unsigned int i, j;
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      if (j == i) {
        mx[i][j] = val;
      } else {
        mx[i][j] = 0;
      }
    }
  }
}

void ones(int r, int c, float mx[r][c], float val) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) {
      mx[i][j] = val;
    }
  }
}

/* C = A + B */
void mxAdd(int r, int c, float A[r][c], float B[r][c], float C[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) C[i][j] = A[i][j] + B[i][j];
  }
}

/* m1 -= m2 */
void mxSub(int r, int c, float A[r][c], float B[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) A[i][j] -= B[i][j];
  }
}

/* m1 += m2 */
void mxAddEq(int r, int c, float A[r][c], float B[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) A[i][j] += B[i][j];
  }
}

/* A = B */
void writeOver(int r, int c, float A[r][c], float B[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) A[i][j] = B[i][j];
  }
}

void write1Row(int n, int r, int c, float vec[1][c], float mx[r][c]) {
  unsigned int i;
  for (i = 0; i < c; i++){
    mx[n][i] = vec[0][i];
  }
}

void write1RowScalar(int n, int r, int c, float scalar, float mx[r][c]) {
  unsigned int i;
  for (i = 0; i < c; i++) {
    mx[n][i] = scalar;
  }
}

void multiplyRowByVect(int n, int r, int c, float vec[1][c], float mx[r][c]) {
  unsigned int i;
  for (i = 0; i < c; i++) {
    mx[n][i] *= vec[0][i];
  }
}

/* n -> row number, r -> row dimension, c -> column dimension */
void multiplyRowByScalar(int n, int r, int c, float scalar, float mx[r][c]) {
  unsigned int i;
  for (i = 0; i < c; i++) {
    mx[n][i] *= scalar;
  }
}

void scalarMultiply(int r, int c, float mx[r][c], float val) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) {
      mx[i][j] *= val;
    }
  }
}

void mxPow2(int r, int c, float mx[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) {
      mx[i][j] *= mx[i][j];
    }
  }
}

void mxElemMul(int r, int c, float A[r][c], float B[r][c], float C[r][c]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) {
      C[i][j] = A[i][j] * B[i][j];
    }
  }
}

void slice2D(int r, int c, int startRow, int stopRow, int startCol, int stopCol, float in[r][c], float out[stopRow - startRow][stopCol - startCol]) {
  unsigned int i, j;
  for (i = 0; i < (stopRow - startRow); i++) {
    for (j = 0; j < (stopCol - startCol); j++) {
      out[i][j] = in[i+startRow][j+startCol];
    }
  }
}

void mxMul(int rA, int cA, float A[rA][cA], int rB, int cB, float B[rB][cB], float C[rA][cB]) {
  unsigned int i, j, k;

  // Initializing elements of matrix mult to 0.
  for (i = 0; i < rA; ++i) {
    for (j = 0; j < cB; ++j) {
      C[i][j] = 0;
    }
  }
  // Multiplying first and second matrices and storing in mult.
  for (i = 0; i < rA; ++i) {
    for (j = 0; j < cB; ++j) {
      for (k = 0; k < cA; ++k) {
        C[i][j] += A[i][k] * B[k][j];
      }
    }
  }
}

void transpose(int r, int c, float in[r][c], float out[c][r]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < c; j++) {
      out[j][i] = in[i][j];
    }
  }
}

/* concatenate rows */
void concat_0(int rA, int rB, int c, float A[rA][c], float B[rB][c], float C[rA + rB][c]) {
  unsigned int i, j;
  for (i = 0; i < rA; i++) {
    for (j = 0; j < c; j++) {
      C[i][j] = A[i][j];
    }
  }
  for (i = rA; i < rA + rB; i++) {
    for (j = 0; j < c; j++) {
      C[i][j] = B[i - rA][j];
    }
  }
}

/* concatenate columns */
void concat_1(int r, int cA, int cB, float A[r][cA], float B[r][cB], float C[r][cA + cB]) {
  unsigned int i, j;
  for (i = 0; i < r; i++) {
    for (j = 0; j < cA; j++) {
      C[i][j] = A[i][j];
    }
  }
  for (i = 0; i < r; i++) {
    for (j = cA; j < cA + cB; j++) {
      C[i][j] = B[i][j - cA];
    }
  }
}

/* Gauss Jordan Elimination */
void invert(int size, float in[size][size], float out[size][size]) {
  float factor;
  int i, j, k;

  float I[size][size];
  identity(size, I, 1);

  /* concatenate identity matrix */
  float tmp[size][2 * size];
  concat_1(size, size, size, in, I, tmp);

  /* Applying Gauss Jordan Elimination */
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      if (i != j) {
        factor = tmp[j][i] / tmp[i][i];
        for (k = 0; k < 2 * size; k++) {
          tmp[j][k] = tmp[j][k] - factor * tmp[i][k];
        }
      }
    }
  }

  /* Row Operation to Make Principal Diagonal to 1 */
  for (i = 0; i < size; i++) {
    for (j = size; j < 2 * size; j++) {
      tmp[i][j] = tmp[i][j] / tmp[i][i];
    }
  }

  /* discard identity matrix */
  for (i = 0; i < size; i++) {
    for (j = size; j < 2 * size; j++) {
      out[i][j - size] = tmp[i][j];
    }
  }

}

void mxInverse(int size, float in[size][size], float out[size][size]) {
  unsigned int i, j, l;
  float factor;

  identity(size, out, 1);

  /* reduce each of the rows to get a lower triangle */
  for (i = 0; i < size; i++) {
    for (j = i + 1; j < size; j++) {
      if (in[i][i] == 0) {
        for (l = i + 1; l < size; l++) {
          if (in[l][l] != 0) {
            row_swap(size, in, i, l);
            break;
          }
        }
        continue;
      }
      factor = in[i][j] / (in[i][i]);
      reduce(size, out, i, j, factor);
      reduce(size, in, i, j, factor);
    }
  }
  /* now finish the upper triangle  */
  for (i = size - 1; i > 0; i--) {
    for (j = i - 1; j >= 0; j--) {
      if (in[i][i] == 0) continue;
      if (j == -1) break;
      factor = in[i][j] / (in[i][i]);
      reduce(size, out, i, j, factor);
      reduce(size, in, i, j, factor);
    }
  }
  /* scale everything to 1 */
  for (i = 0; i < size; i++) {
    if (in[i][i] == 0) continue;
    factor = 1 / (in[i][i]);
    row_scalar_multiply(size, out, i, factor);
    row_scalar_multiply(size, in, i, factor);
  }
}

int row_swap(int size, float mx[size][size], int a, int b) {
  float temp;
  unsigned int i;

  for (i = 0; i < size; i++) {
    temp = mx[i][a];
    mx[i][a] = mx[i][b];
    mx[i][b] = temp;
  }
  return SUCC;
}

/* reduce row b by factor*a  */
int reduce(int size, float mx[size][size], int a, int b, float factor) {
  int i;

  for (i = 0; i < size; i++) {
    mx[i][b] -= mx[i][a] * factor;
  }

  return SUCC;
}

static int row_scalar_multiply(int size, float mx[size][size], int row, float factor) {
  int i;

  for (i = 0; i < size; i++) mx[i][row] *= factor;
  return SUCC;
}

//---------------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root

float invSqrt(float x) {
float xhalf = 0.5f*x;
  uint32_t i;
  assert(sizeof(x) == sizeof(i));
  memcpy(&i, &x, sizeof(i));
  i = 0x5f375a86 - (i>>1);
  memcpy(&x, &i, sizeof(i));
  x = x*(1.5f - xhalf*x*x);
  return x;
}


/* MATRIX INVERSE */

// Function to get cofactor of A[p][q] in temp[][]. n is current
// dimension of A[][]
void getCofactor(int N, float A[N][N], float temp[N][N], int p, int q, int n) {
  int i = 0, j = 0;

  // Looping for each element of the matrix
  for (int row = 0; row < n; row++) {
    for (int col = 0; col < n; col++) {
      //  Copying into temporary matrix only those element
      //  which are not in given row and column
      if (row != p && col != q) {
        temp[i][j++] = A[row][col];

        // Row is filled, so increase row index and
        // reset col index
        if (j == n - 1) {
          j = 0;
          i++;
        }
      }
    }
  }
}

/* Recursive function for finding determinant of matrix.
   n is current dimension of A[][]. */
float determinant(int N, float A[N][N], int n) {
  float D = 0;  // Initialize result

  //  Base case : if matrix contains single element
  if (n == 1) return A[0][0];

  float temp[N][N];  // To store cofactors

  int sign = 1;  // To store sign multiplier

  // Iterate for each element of first row
  for (int f = 0; f < n; f++) {
    // Getting Cofactor of A[0][f]
    getCofactor(N, A, temp, 0, f, n);
    D += sign * A[0][f] * determinant(N, temp, n - 1);

    // terms are to be added with alternate sign
    sign = -sign;
  }

  return D;
}

// Function to get adjoint of A[N][N] in adj[N][N].
void adjoint(int N, float A[N][N], float adj[N][N]) {
  if (N == 1) {
    adj[0][0] = 1;
    return;
  }

  // temp is used to store cofactors of A[][]
  int sign = 1;
  float temp[N][N];

  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      // Get cofactor of A[i][j]
      getCofactor(N, A, temp, i, j, N);

      // sign of adj[j][i] positive if sum of row
      // and column indexes is even.
      sign = ((i + j) % 2 == 0) ? 1 : -1;

      // Interchanging rows and columns to get the
      // transpose of the cofactor matrix
      adj[j][i] = (sign) * (determinant(N, temp, N - 1));
    }
  }
}

// Function to calculate and store inverse, returns false if
// matrix is singular
int inverse(int N, float A[N][N], float inverse[N][N]) {
  // Find determinant of A[][]
  int det = determinant(N, A, N);
  if (det == 0) {
    return -1;
  }

  // Find adjoint
  float adj[N][N];
  adjoint(N, A, adj);

  // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++) inverse[i][j] = adj[i][j] / det;

  return 1;
}